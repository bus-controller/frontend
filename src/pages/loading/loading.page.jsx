import {
    Box,
    LinearProgress,
    colors,
} from "@mui/material";

const LoadingPage = () => {
    return (
        <Box
            sx={{
                display: "flex",
                alignItems: "center",
                justifyContent: "center",
                height: "100vh",
            }}
        >
            <Box sx={{ textAlign: "center" }}>
                <Box
                    src={"https://www.shahabkhodro.co.ir/wp-content/themes/shahabkhodro/assets/images/logo.png"}
                    component="img"
                    sx={{
                        mb: 5,
                    }}
                />
                <LinearProgress sx={{ color: colors.blue[900] }} />
            </Box>
        </Box>
    );
}

export default LoadingPage;