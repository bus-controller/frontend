import { useState, useEffect } from "react";
import { useParams } from "react-router-dom";

import {
    Box,
    Grid,
    Table,
    TableContainer,
    TableHead,
    TableBody,
    TableRow,
    TableCell,
    Paper,
    Button,
    Typography,
} from "@mui/material";

import Axios from "axios";
import * as XLSX from "xlsx";

import LoadingComponent from "../../../components/loading.component";
import ShowSnackbar from "../../../components/snackbar.component";

import { persianNumeric } from "../../../hooks/most.hook";

const ReportLogTab = () => {
    const { project_id } = useParams();

    const env = process.env;
    const baseUrl = env.REACT_APP_BACKEND_URL;

    const [project, setProject] = useState("");
    const [busses, setBusses] = useState("");

    // eslint-disable-next-line
    const [spent, setSpent] = useState(0);
    const [budget, setBudget] = useState(0);

    const [openSnack, setOpenSnack] = useState(false);
    const [openSnackMessage, setOpenSnackMessage] = useState("");

    useEffect(() => {
        Axios.post(`${baseUrl}/logs/backup/project`, { id: project_id })
            .then((result) => {
                const { project, busses } = result.data;

                setBudget(project.budget);
                setSpent(
                    project.sazehBudgetSpent + project.niroBudgetSpent + project.barghBudgetSpent + project.tazinatBudgetSpent + project.colorBudgetSpent + project.etcBudgetSpent
                );

                setProject(project);
                setBusses(busses);
            })
            .catch((error) => {
                setOpenSnackMessage("متاسفانه مشکلی رخ داده است");                
                setOpenSnack(true);
            });
    }, [baseUrl, project_id]);

    const convertExcel = () => {
        const wb = XLSX.utils.book_new();

        const projectSheet = XLSX.utils.table_to_sheet(document.getElementById('budget'));
        XLSX.utils.book_append_sheet(wb, projectSheet, "پروژه");

        // const busesSheet = XLSX.utils.sheet_add_dom(busses.reverse());
        const busesSheet = XLSX.utils.table_to_sheet(document.getElementById('busses'));
        XLSX.utils.book_append_sheet(wb, busesSheet, "اتوبوس ها");

        XLSX.writeFile(wb, `${project.name}.xlsx`);

        setOpenSnackMessage("گزارش گرفته شد");
        setOpenSnack(true);
    }

    return (
        <Box sx={{ px: 2, py: 1 }}>
            {
                project !== ""
                ?
                <Box>
                    <Button
                        color="success"
                        variant="contained"
                        sx={{
                            mb: 2,
                        }}
                        disabled={busses.length !== 0 ? false : true}
                        onClick={() => convertExcel()}
                        disableElevation
                    >
                        خروجی فایل اکسل
                    </Button>
                    <Grid
                        spacing={2}
                        container
                    >
                        <Grid md={8} item>
                            <Box>
                                {
                                    busses.length !== 0
                                    ?
                                    <TableContainer component={Paper} variant="outlined">
                                        <Table id="busses">
                                            <TableHead>
                                                <TableRow sx={{ bgcolor: "action.hover" }}>
                                                    <TableCell sx={{ fontWeight: "bold" }} align="center">پلاک</TableCell>
                                                    <TableCell sx={{ fontWeight: "bold" }} align="center">وضعیت</TableCell>
                                                    <TableCell sx={{ fontWeight: "bold" }} align="center">توضیحات</TableCell>
                                                </TableRow>
                                            </TableHead>
                                            <TableBody>
                                                {
                                                    busses.map((bus) => (
                                                        <TableRow
                                                            key={bus._id}
                                                            sx={{
                                                                '&:nth-of-type(odd)': {
                                                                    backgroundColor: "action.hover",
                                                                },
                                                            }}
                                                        >
                                                            <TableCell align="center">{ bus.plaque }</TableCell>
                                                            <TableCell align="center">{ bus.status }</TableCell>
                                                            <TableCell align="center">{ bus.details }</TableCell>
                                                        </TableRow>
                                                    ))
                                                }
                                            </TableBody>
                                        </Table>
                                    </TableContainer>
                                    :
                                    <Typography>
                                        اتوبوس ها خالی میباشد
                                    </Typography>
                                }
                            </Box>
                        </Grid>
                        <Grid md={4} item>
                            <Box>
                                <TableContainer component={Paper} variant="outlined">
                                    <Table id="budget">
                                        <TableHead>
                                            <TableRow sx={{ bgcolor: "action.hover" }}>
                                                <TableCell colSpan={4} align="center" sx={{ fontWeight: "bold" }}>{ project.name }</TableCell>
                                            </TableRow>
                                            <TableRow>
                                                <TableCell colSpan={2} align="center">کل بودجه مصوب</TableCell>
                                                <TableCell colSpan={2} align="center">{ persianNumeric(project.budget) }</TableCell>
                                            </TableRow>
                                        </TableHead>
                                        <TableBody>
                                            <TableRow>
                                                <TableCell align="center">شرح</TableCell>
                                                <TableCell align="center">بودجه مصوب</TableCell>
                                                <TableCell align="center">هزینه تا کنون</TableCell>
                                                <TableCell align="center">مانده</TableCell>
                                            </TableRow>
                                            <TableRow>
                                                <TableCell align="center">سازه</TableCell>
                                                <TableCell align="center">{ persianNumeric(project.sazehBudget) }</TableCell>
                                                <TableCell align="center">{ persianNumeric(project.sazehBudgetSpent) }</TableCell>
                                                <TableCell align="center">{ persianNumeric(project.sazehBudget - project.sazehBudgetSpent) }</TableCell>
                                            </TableRow>
                                            <TableRow>
                                                <TableCell align="center">نیرو محرکه</TableCell>
                                                <TableCell align="center">{ persianNumeric(project.niroBudget) }</TableCell>
                                                <TableCell align="center">{ persianNumeric(project.niroBudgetSpent) }</TableCell>
                                                <TableCell align="center">{ persianNumeric(project.niroBudget - project.niroBudgetSpent) }</TableCell>
                                            </TableRow>
                                            <TableRow>
                                                <TableCell align="center">برق</TableCell>
                                                <TableCell align="center">{ persianNumeric(project.barghBudget) }</TableCell>
                                                <TableCell align="center">{ persianNumeric(project.barghBudgetSpent) }</TableCell>
                                                <TableCell align="center">{ persianNumeric(project.barghBudget - project.barghBudgetSpent) }</TableCell>
                                            </TableRow>
                                            <TableRow>
                                                <TableCell align="center">تزئینات</TableCell>
                                                <TableCell align="center">{ persianNumeric(project.tazinatBudget) }</TableCell>
                                                <TableCell align="center">{ persianNumeric(project.tazinatBudgetSpent) }</TableCell>
                                                <TableCell align="center">{ persianNumeric(project.tazinatBudget - project.tazinatBudgetSpent) }</TableCell>
                                            </TableRow>
                                            <TableRow>
                                                <TableCell align="center">رنگ</TableCell>
                                                <TableCell align="center">{ persianNumeric(project.colorBudget) }</TableCell>
                                                <TableCell align="center">{ persianNumeric(project.colorBudgetSpent) }</TableCell>
                                                <TableCell align="center">{ persianNumeric(project.colorBudget - project.colorBudgetSpent) }</TableCell>
                                            </TableRow>
                                            <TableRow>
                                                <TableCell align="center">غیره</TableCell>
                                                <TableCell align="center">{ persianNumeric(project.etcBudget) }</TableCell>
                                                <TableCell align="center">{ persianNumeric(project.etcBudgetSpent) }</TableCell>
                                                <TableCell align="center">{ persianNumeric(project.etcBudget - project.etcBudgetSpent) }</TableCell>
                                            </TableRow>
                                            <TableRow>
                                                <TableCell align="center" sx={{ color: "red", fontWeight: "bold", fontSize: "120%" }}>جمع کل</TableCell>
                                                <TableCell align="center" sx={{ color: "red", fontWeight: "bold", fontSize: "120%" }}>{ persianNumeric(budget) }</TableCell>
                                                <TableCell align="center" sx={{ color: "red", fontWeight: "bold", fontSize: "120%" }}>{ persianNumeric(spent) }</TableCell>
                                                <TableCell align="center" sx={{ color: "red", fontWeight: "bold", fontSize: "120%" }}>{ persianNumeric(budget - spent) }</TableCell>
                                            </TableRow>
                                        </TableBody>
                                    </Table>
                                </TableContainer>
                            </Box>
                        </Grid>
                    </Grid>
                </Box>
                :
                <LoadingComponent />
            }

            <ShowSnackbar
                open={openSnack}
                close={() => setOpenSnack(false)}
                message={openSnackMessage}
            />
        </Box>
    );
}

export default ReportLogTab;