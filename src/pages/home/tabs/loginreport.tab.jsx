import { useState, useEffect } from "react";

import {
    Box,
    Table,
    TableContainer,
    TableHead,
    TableBody,
    TableRow,
    TableCell,
    Paper,
    colors,
    Button,
} from "@mui/material";

import Axios from "axios";
import * as XLSX from "xlsx";

import LoadingComponent from "../../../components/loading.component";
import ShowSnackbar from "../../../components/snackbar.component";

const LoginLogTab = () => {
    const env = process.env;
    const baseUrl = env.REACT_APP_BACKEND_URL;

    const [busLogs, setBusLogs] = useState("");

    const [openSnack, setOpenSnack] = useState(false);
    const [openSnackMessage, setOpenSnackMessage] = useState("");

    useEffect(() => {
        Axios.get(`${baseUrl}/logs/login/logs`)
            .then((result) => {
                setBusLogs(result.data.logs.reverse());
            })
            .catch((error) => {
                setOpenSnackMessage("متاسفانه مشکلی رخ داده است");                
                setOpenSnack(true);           });
    }, [baseUrl]);

    const convertExcel = () => {
        const wb = XLSX.utils.book_new();

        const ws = XLSX.utils.table_to_sheet(document.getElementById('logintable'));
        XLSX.utils.book_append_sheet(wb, ws, "گزارش ورود و خرج ها");

        XLSX.writeFile(wb, 'login-logs.xlsx');

        setOpenSnackMessage("خروجی گرفته شد");
        setOpenSnack(true);
    }

    return (
        <Box sx={{ px: 2, py: 1 }}>
            {
                busLogs !== ""
                ?
                <Box>
                    <Button
                        color="success"
                        variant="contained"
                        sx={{
                            mb: 2,
                        }}
                        onClick={() => convertExcel()}
                        disableElevation
                    >
                        خروجی فایل اکسل
                    </Button>
                    <TableContainer component={Paper} variant="outlined">
                        <Table id="logintable">
                            <TableHead>
                                <TableRow>
                                    <TableCell align="center">نوع</TableCell>
                                    <TableCell align="center">کاربر</TableCell>
                                    <TableCell align="center">تاریخ</TableCell>
                                    <TableCell align="center">زمان</TableCell>
                                    <TableCell align="center">آیپی</TableCell>
                                </TableRow>
                            </TableHead>
                            <TableBody>
                                {
                                    busLogs.map((log) => (
                                        <TableRow
                                            key={log._id}
                                            sx={{
                                                bgcolor: log.type === "login" ? colors.green[200] : colors.red[200],
                                            }}
                                        >
                                            <TableCell align="center">{ log.type === "login" ? "ورود" : "خروج" }</TableCell>
                                            <TableCell align="center">{ log.user }</TableCell>
                                            <TableCell align="center">{ log.date }</TableCell>
                                            <TableCell align="center">{ log.time }</TableCell>
                                            <TableCell align="center" dir="ltr">{ log.ip }</TableCell>
                                        </TableRow>
                                    ))
                                }
                            </TableBody>
                        </Table>
                    </TableContainer>
                </Box>
                :
                <LoadingComponent />
            }

            <ShowSnackbar
                open={openSnack}
                close={() => setOpenSnack(false)}
                message={openSnackMessage}
            />
        </Box>
    );
}

export default LoginLogTab;