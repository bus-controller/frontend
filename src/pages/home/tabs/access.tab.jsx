import { useState, useEffect } from "react";

import {
    Box,
    List,
    ListItem,
    ListItemText,
    ListItemButton,
} from "@mui/material";

import Axios from "axios";

import ShowSnackbar from "../../../components/snackbar.component";

import UpdateUserDialog from "../../../dialogs/update/updateuser.dialog";
import LoadingComponent from "../../../components/loading.component";

const AccessTab = () => {
    const [users, setUsers] = useState("");

    const env = process.env;
    const baseUrl = env.REACT_APP_BACKEND_URL;

    const [id, setId] = useState("");
    const [openUpdate, setOpenUpdate] = useState(false);

    const [openSnack, setOpenSnack] = useState(false);
    const [openSnackMessage, setOpenSnackMessage] = useState("");

    const UpdateData = () => {
        Axios.get(`${baseUrl}/user/all`)
            .then((result) => {
                 setUsers(result.data.users);
            })
            .catch((error) => {
                setOpenSnackMessage("متاسفانه مشکلی رخ داده است");                
                setOpenSnack(true);
           });
    }

    useEffect(() => {
        Axios.post(`${baseUrl}/user/all`)
            .then((result) => {
                setUsers(result.data.users);
            })
            .catch((error) => {
                setOpenSnackMessage("متاسفانه مشکلی رخ داده است");                
                setOpenSnack(true);
           });
    }, [baseUrl]);

    return (
        <Box>
            {
                users !== ""
                ?
                <List>
                    {
                        users.map((user) => (
                            <ListItem key={user._id} divider disablePadding>
                                <ListItemButton
                                    onClick={() => {
                                        setId(user._id)
                                        setOpenUpdate(true);
                                    }}
                                >
                                    <ListItemText
                                        sx={{
                                            textAlign: "right",
                                        }}
                                        primary={user.name}
                                    />
                                </ListItemButton>
                            </ListItem>
                        ))
                    }
                </List>
                :
                <LoadingComponent />
            }

            <ShowSnackbar
                open={openSnack}
                close={() => setOpenSnack(false)}
                message={openSnackMessage}
            />

            <UpdateUserDialog open={openUpdate} close={() => {setOpenUpdate(false); UpdateData();}} id={id} />
        </Box>
    );
}

export default AccessTab;