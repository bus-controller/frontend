import { useState, useEffect } from "react";

import {
    Box,
    Table,
    TableContainer,
    TableHead,
    TableBody,
    TableRow,
    TableCell,
    Paper,
    Button,
} from "@mui/material";

import Axios from "axios";
import * as XLSX from "xlsx";

import LoadingComponent from "../../../components/loading.component";
import ShowSnackbar from "../../../components/snackbar.component";

const BusLogsTab = () => {
    const env = process.env;
    const baseUrl = env.REACT_APP_BACKEND_URL;

    const [busLogs, setBusLogs] = useState("");

    const [openSnack, setOpenSnack] = useState(false);
    const [openSnackMessage, setOpenSnackMessage] = useState("");

    useEffect(() => {
        Axios.get(`${baseUrl}/logs/bus/logs`)
            .then((result) => {
                setBusLogs(result.data.logs.reverse());
            })
            .catch((error) => {
                setOpenSnackMessage("متاسفانه مشکلی رخ داده است");                
                setOpenSnack(true);
           });
    }, [baseUrl]);

    const convertExcel = () => {
        const wb = XLSX.utils.book_new();

        const ws = XLSX.utils.table_to_sheet(document.getElementById('bussestable'));
        XLSX.utils.book_append_sheet(wb, ws, "گزارش اتوبوس ها");

        XLSX.writeFile(wb, 'bus-logs.xlsx');

        setOpenSnackMessage("خروجی گرفته شد");
        setOpenSnack(true);
    }

    return (
        <Box sx={{ px: 2, py: 1 }}>
            {
                busLogs !== ""
                ?
                <Box>
                    <Button
                        color="success"
                        variant="contained"
                        sx={{
                            mb: 2,
                        }}
                        onClick={() => convertExcel()}
                        disableElevation
                    >
                        خروجی فایل اکسل
                    </Button>
                    <TableContainer component={Paper} variant="outlined">
                        <Table id="bussestable">
                            <TableHead>
                                <TableRow>
                                    <TableCell align="center">پروژه</TableCell>
                                    <TableCell align="center">کاربر</TableCell>
                                    <TableCell align="center">اتوبوس</TableCell>
                                    <TableCell align="center">از بخش</TableCell>
                                    <TableCell align="center">به بخش</TableCell>
                                    <TableCell align="center">تاریخ</TableCell>
                                    <TableCell align="center">زمان</TableCell>
                                    <TableCell align="center">پیام</TableCell>
                                </TableRow>
                            </TableHead>
                            <TableBody>
                                {
                                    busLogs.map((log) => (
                                        <TableRow
                                            key={log._id}
                                            sx={{
                                                '&:nth-of-type(odd)': {
                                                    backgroundColor: "action.hover",
                                                },
                                            }}
                                        >
                                            <TableCell align="center">{ log.project }</TableCell>
                                            <TableCell align="center">{ log.user }</TableCell>
                                            <TableCell align="center">{ log.bus }</TableCell>
                                            <TableCell align="center">{ log.from }</TableCell>
                                            <TableCell align="center">{ log.to }</TableCell>
                                            <TableCell align="center">{ log.date }</TableCell>
                                            <TableCell align="center">{ log.time }</TableCell>
                                            <TableCell align="center">{ log.message }</TableCell>
                                        </TableRow>
                                    ))
                                }
                            </TableBody>
                        </Table>
                    </TableContainer>
                </Box>
                :
                <LoadingComponent />
            }

            <ShowSnackbar
                open={openSnack}
                close={() => setOpenSnack(false)}
                message={openSnackMessage}
            />
        </Box>
    );
}

export default BusLogsTab;