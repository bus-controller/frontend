import { useState, useEffect } from "react";

import {
    Box,
    List,
    ListItem,
    ListItemText,
    ListItemButton,
} from "@mui/material";

import Axios from "axios";

import ShowSnackbar from "../../../components/snackbar.component";

import UpdateStatusDialog from "../../../dialogs/update/updatestatus.dialog";
import LoadingComponent from "../../../components/loading.component";

const StatusesTab = () => {
    const [statuses, setStatuses] = useState("");

    const env = process.env;
    const baseUrl = env.REACT_APP_BACKEND_URL;

    const [id, setId] = useState("");
    const [openUpdate, setOpenUpdate] = useState(false);

    const [openSnack, setOpenSnack] = useState(false);
    const [openSnackMessage, setOpenSnackMessage] = useState("");

    const UpdateData = () => {
        Axios.get(`${baseUrl}/status/all`)
            .then((result) => {
                setStatuses(result.data.statuses);
            })
            .catch((error) => {
                setOpenSnackMessage("متاسفانه مشکلی رخ داده است");                
                setOpenSnack(true);
           });
    }

    useEffect(() => {
        Axios.get(`${baseUrl}/status/all`)
            .then((result) => {
                setStatuses(result.data.statuses);
            })
            .catch((error) => {
                setOpenSnackMessage("متاسفانه مشکلی رخ داده است");                
                setOpenSnack(true);
           });
    }, [baseUrl]);

    return (
        <Box>
            {
                statuses !== ""
                ?
                <List>
                    {
                        statuses.map((status) => (
                            <ListItem key={status._id} divider disablePadding>
                                <ListItemButton
                                    onClick={() => {
                                        setId(status._id)
                                        setOpenUpdate(true);
                                    }}
                                >
                                    <ListItemText
                                        sx={{
                                            textAlign: "right",
                                        }}
                                        primary={status.sort + ' | ' + status.label}
                                    />
                                </ListItemButton>
                            </ListItem>
                        ))
                    }
                </List>
                :
                <LoadingComponent />
            }

            <ShowSnackbar
                open={openSnack}
                close={() => setOpenSnack(false)}
                message={openSnackMessage}
            />

            <UpdateStatusDialog open={openUpdate} close={() => {setOpenUpdate(false); UpdateData();}} id={id} />
        </Box>
    );
}

export default StatusesTab;