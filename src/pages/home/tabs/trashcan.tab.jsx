import { useState, useEffect } from "react";
import { useSelector } from "react-redux";

import {
    Box,
    Typography,
    Tooltip,
    List,
    ListItem,
    ListItemText,
    ListItemButton,
    Grid,
    Divider,
    Toolbar,
} from "@mui/material";

import Axios from "axios";

import ShowSnackbar from "../../../components/snackbar.component";

import UpdateBusDialog from "../../../dialogs/update/updatebus.dialog";
import UpdateBudget from "../../../dialogs/update/updatebudget.dialog";

import LoadingComponent from "../../../components/loading.component";

const DeletedBusesTab = () => {
    const [buses, setBuses] = useState("");
    const [statuses, setStatuses] = useState("");
    const [cities, setCities] = useState("");
    
    const env = process.env;
    const baseUrl = env.REACT_APP_BACKEND_URL;

    const [busID, setBusID] = useState("");
    const [cityID, setCityID] = useState("");

    const [openUpdate, setOpenUpdate] = useState(false);
    const [openUpdateBudget, setOpenUpdateBudget] = useState(false);

    const [openSnack, setOpenSnack] = useState(false);
    const [openSnackMessage, setOpenSnackMessage] = useState("");

    const access = useSelector(state => state.user);

    const UpdateData = () => {
        Axios.get(`${baseUrl}/bus/all/deleted`)
            .then((result) => {
                 setBuses(result.data.buses);
            })
            .catch((error) => {
                setOpenSnackMessage("متاسفانه مشکلی رخ داده است");                
                setOpenSnack(true);
           });

        Axios.get(`${baseUrl}/status/all`)
            .then((result) => {
                setStatuses(result.data.statuses);
            })
            .catch((error) => {
                setOpenSnackMessage("متاسفانه مشکلی رخ داده است");                
                setOpenSnack(true);
            });
    }

    const updatingBus = (id) => {
        setBusID(id);
        setOpenUpdate(true);
    }

    const updatingCity = (id) => {
        setCityID(id);
        setOpenUpdateBudget(true);
    }

    useEffect(() => {
        Axios.get(`${baseUrl}/bus/all/deleted`)
            .then((result) => {
                 setBuses(result.data.buses);
            })
            .catch((error) => {
                setOpenSnackMessage("متاسفانه مشکلی رخ داده است");                
                setOpenSnack(true);
           });

        Axios.get(`${baseUrl}/status/all`)
           .then((result) => {
                setStatuses(result.data.statuses);
           })
           .catch((error) => {
                setOpenSnackMessage("متاسفانه مشکلی رخ داده است");                
                setOpenSnack(true);
          });

        Axios.get(`${baseUrl}/city/all`)
            .then((result) => {
                setCities(result.data.cities);
            })
            .catch((error) => {
                setOpenSnackMessage("متاسفانه مشکلی رخ داده است");                
                setOpenSnack(true);
            });
    }, [baseUrl]);

    return (
        <Box>
            <Toolbar>
                {
                    cities !== ""
                    ?
                    <Grid
                        spacing={1}
                        container
                    >
                        {
                            cities.map((city) => (
                                <Grid key={city._id} item>
                                    <Box
                                        sx={{
                                            px: 0.25,
                                            py: 1,
                                            width: 196,
                                            textAlign: "center",
                                            bgcolor: city.color,
                                            cursor: "pointer",
                                        }}
                                        onClick={() => updatingCity(city._id)}
                                    >
                                        <Typography
                                            sx={{
                                                color: "black"
                                            }}
                                        >
                                            { city.name }
                                        </Typography>
                                    </Box>
                                </Grid>
                            ))
                        }
                    </Grid>
                    :
                    <LoadingComponent />
                }
            </Toolbar>
            <Divider sx={{ mb: 1 }} />
            {
                statuses !== ""
                ?
                <Grid
                    container
                >
                    {
                        statuses.map((status) => (
                            <Grid key={status._id} item>
                                <Box
                                    sx={{
                                        px: 0.25,
                                        width: 196,
                                        textAlign: "center",
                                    }}
                                >
                                    <Typography
                                        variant="body1"
                                        fontSize="115%"
                                        fontFamily="Titr"
                                        sx={{
                                            mb: 1,
                                            bgcolor: "black",
                                            color: "white",
                                            p: 1,
                                        }}
                                        gutterBottom
                                    >
                                        { status.label }
                                    </Typography>
                                    <Divider sx={{ mb: -1 }} />
                                    <List>
                                        {
                                            buses !== ""
                                            ?
                                            buses.filter((bus) => bus.status === status.name).map((bus) => (
                                                <Tooltip
                                                    key={bus._id}
                                                    title={
                                                        <Typography
                                                            variant="h5"
                                                            sx={{
                                                                direction: "rtl",
                                                                p: 1,
                                                            }}
                                                        >
                                                            { bus.details }
                                                        </Typography>
                                                    }
                                                    arrow
                                                >
                                                    <ListItem disablePadding dense sx={{ py: 0.25 }}>
                                                        <ListItemButton
                                                            onClick={ access === "viewer" ? () => {} : () => updatingBus(bus._id) }
                                                            sx={{
                                                                bgcolor: bus.color,
                                                                "&:hover": {
                                                                    opacity: "0.7",
                                                                    bgcolor: bus.color,
                                                                }
                                                            }}
                                                        >
                                                            <ListItemText
                                                                sx={{
                                                                    textAlign: "center",
                                                                }}
                                                                primary={
                                                                    <Typography
                                                                        fontSize="125%"
                                                                        sx={{
                                                                            color: "black"
                                                                        }}
                                                                    >
                                                                        { bus.plaque }
                                                                    </Typography>
                                                                }
                                                            />
                                                        </ListItemButton>
                                                    </ListItem>
                                                </Tooltip>
                                            ))
                                            :
                                            <LoadingComponent />
                                        }
                                    </List>
                                </Box>
                            </Grid>
                        ))
                    }
                </Grid>
                :
                <LoadingComponent />
            }

            <ShowSnackbar
                open={openSnack}
                close={() => setOpenSnack(false)}
                message={openSnackMessage}
            />

            <UpdateBusDialog open={openUpdate} close={() => {setOpenUpdate(false); UpdateData();}} id={busID} />
            <UpdateBudget open={openUpdateBudget} close={() => {setOpenUpdateBudget(false); UpdateData();}} id={cityID} />
        </Box>
    );
}

export default DeletedBusesTab;