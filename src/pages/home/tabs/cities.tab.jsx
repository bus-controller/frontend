import { useState, useEffect } from "react";

import {
    Box,
    ListItem,
    ListItemText,
    ListItemButton,
} from "@mui/material";

import {
    Masonry,
} from "@mui/lab";

import Axios from "axios";

import ShowSnackbar from "../../../components/snackbar.component";

import UpdateCityDialog from "../../../dialogs/update/updatecity.dialog";
import LoadingComponent from "../../../components/loading.component";

const CitiesTab = () => {
    const [cities, setCities] = useState("");

    const env = process.env;
    const baseUrl = env.REACT_APP_BACKEND_URL;

    const [id, setId] = useState("");
    const [openUpdate, setOpenUpdate] = useState(false);

    const [openSnack, setOpenSnack] = useState(false);
    const [openSnackMessage, setOpenSnackMessage] = useState("");

    const UpdateData = () => {
        Axios.get(`${baseUrl}/city/all`)
            .then((result) => {
                setCities(result.data.cities);
            })
            .catch((error) => {
                setOpenSnackMessage("متاسفانه مشکلی رخ داده است");                
                setOpenSnack(true);           });
    }

    useEffect(() => {
        Axios.get(`${baseUrl}/city/all`)
            .then((result) => {
                setCities(result.data.cities);
            })
            .catch((error) => {
                setOpenSnackMessage("متاسفانه مشکلی رخ داده است");                
                setOpenSnack(true);           });
    }, [baseUrl]);

    return (
        <Box>
            {
                cities !== ""
                ?
                <Masonry
                    spacing={2}
                    columns={4}
                >
                    {
                        cities.map((city) => (
                            <ListItem key={city._id}>
                                <ListItemButton
                                    onClick={() => {
                                        setId(city._id)
                                        setOpenUpdate(true);
                                    }}
                                    sx={{
                                        bgcolor: city.color,
                                        '&:hover': {
                                            bgcolor: city.color,
                                        }
                                    }}
                                >
                                    <ListItemText
                                        sx={{
                                            color: "black",
                                            textAlign: "center",
                                        }}
                                        primary={city.name}
                                    />
                                </ListItemButton>
                            </ListItem>
                        ))
                    }
                </Masonry>
                :
                <LoadingComponent />
            }

            <ShowSnackbar
                open={openSnack}
                close={() => setOpenSnack(false)}
                message={openSnackMessage}
            />

            <UpdateCityDialog open={openUpdate} close={() => {setOpenUpdate(false); UpdateData();}} id={id} />
        </Box>
    );
}

export default CitiesTab;