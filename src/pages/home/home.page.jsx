import { useState, useEffect } from "react";
import { useSelector } from "react-redux";
import { useHistory } from "react-router-dom";

import {
    Box,
    Divider,
    Toolbar,
} from "@mui/material";

import Axios from "axios";

import ShowSnackbar from "../../components/snackbar.component";
import LoadingComponent from "../../components/loading.component";

import Navbar from "../../components/navbar.component";

const HomePage = (props) => {
    const history = useHistory();

    const { children } = props;

    const session = useSelector(state => state.session);
    !session && history.push("/auth");

    const id = useSelector(state => state.uid);

    const [user, setUser] = useState("");

    const [openSnack, setOpenSnack] = useState(false);
    const [openSnackMessage, setOpenSnackMessage] = useState("");

    const env = process.env;
    const baseUrl = env.REACT_APP_BACKEND_URL;

    useEffect(() => {
        Axios.get(`${baseUrl}/user/user/${id}`)
            .then((result) => {
                setUser(result.data.user);
                
                setOpenSnack(true);
                setOpenSnackMessage("خوش آمدید");
            })
            .catch((error) => {
                setOpenSnack(true);
                setOpenSnackMessage("خطای سرور");
            });
    }, [baseUrl, id]);

    return (
        <Box>
            <Navbar />
            <Box>
                {
                    user !== ""
                    ?
                    <Box>
                        <Toolbar />
                        <Divider sx={{ mb: 1 }} />
                        <Box>
                            { children }
                        </Box>
                    </Box>
                    :
                    <Box
                        sx={{
                            pt: 5,
                            textAlign: "center",
                        }}
                    >
                        <LoadingComponent />
                    </Box>
                }
            </Box>

            <ShowSnackbar
                open={openSnack}
                close={() => setOpenSnack(false)}
                message={openSnackMessage}
            />
        </Box>
    );
}

export default HomePage;