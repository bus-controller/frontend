export const setColor = (color) => {
    return {
        type: "SET_COLOR",
        payload: color,
    }
}