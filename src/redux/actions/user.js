export const setAccess = (access) => {
    return {
        type: "SET_ACCESS",
        payload: access,
    }
}

export const unsertAccess = () => {
    return {
        type: "UNSET_ACCESS",
    }
}