import { combineReducers } from "redux";

import sessionReducer from "./session";
import uidReducer from "./uid";
import userReducer from "./user";
import themeReducer from "./theme";
import colorReducer from "./color";

const allReducers = combineReducers({
    session: sessionReducer,
    uid: uidReducer,
    user: userReducer,
    theme: themeReducer,
    color: colorReducer,
});

export default allReducers;