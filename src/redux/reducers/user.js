const userReducer = (state = "", action) => {
    switch (action.type) {
        case "SET_ACCESS":
            return state = action.payload;
        case "UNSET_ACCESS":
            return state = "";
        default:
            return state;
    }
}

export default userReducer;