const colorReducer = (state = '#008080', action) => {
    switch (action.type) {
        case "SET_COLOR":
            return state = action.payload;
        default:
            return state;
    }
}

export default colorReducer;