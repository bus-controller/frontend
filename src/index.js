import React, { Suspense } from 'react';
import ReactDOM from 'react-dom/client';

import "./App.sass";

import { createStore } from "redux";
import { Provider } from "react-redux";

import allReducers from './redux/reducers';

import { saveState, loadState } from "./redux/localstorage/localstorage";

import App from './App';
import LoadingPage from './pages/loading/loading.page';

const presentedState = loadState();

let store = createStore(
  allReducers,
  presentedState,
  window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__(),
);

store.subscribe(() => saveState({
  uid: store.getState().uid,
  session: store.getState().session,
  user: store.getState().user,
  theme: store.getState().theme,
  color: store.getState().color,
}))

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <Provider store={store}>
    <Suspense fallback={<LoadingPage />}>
      <App />
    </Suspense>
  </Provider>
);