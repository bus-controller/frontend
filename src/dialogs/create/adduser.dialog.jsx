import { useState } from "react";

import {
    Dialog,
    DialogTitle,
    DialogContent,
    DialogActions,
    TextField,
    Button,
    Box,
    Radio,
    RadioGroup,
    FormControl,
    FormLabel,
    FormControlLabel,
} from "@mui/material";

import Axios from "axios";

import ShowSnackbar from "../../components/snackbar.component";

const AddUserDialog = (props) => {
    const { open, close } = props;

    const env = process.env;
    const baseUrl = env.REACT_APP_BACKEND_URL;

    const [loading, setLoading] = useState(false);

    const [openSnack, setOpenSnack] = useState(false);
    const [openSnackMessage, setOpenSnackMessage] = useState("");

    const [name, setName] = useState("");
    const [phone, setPhone] = useState("");
    const [password, setPassword] = useState("");
    const [sex, setSex] = useState("male");
    const [access, setAccess] = useState("admin");

    const addUser = () => {
        setLoading(true);

        const sendData = {
            name,
            phone,
            password,
            sex,
            access,
        };

        Axios.post(`${baseUrl}/auth/register`, sendData)
            .then((result) => {
                setName("");
                setPhone("");
                setPassword("");

                close();

                setLoading(false);

                setOpenSnackMessage(result.data.message);
                setOpenSnack(true);
            })
            .catch((error) => {
                setOpenSnackMessage("متاسفانه مشکلی رخ داده است");
                setOpenSnack(true);

                setLoading(false);
            });
    }

    return (
        <Box>
            <Dialog
                open={open}
                onClose={close}
                sx={{
                    direction: "rtl",
                    textAlign: "right",
                }}
                maxWidth="sm"
                fullWidth
            >
                <DialogTitle
                    color="primary"
                    sx={{
                        color: "primary.main",
                        borderBottom: "solid",
                        borderBottomWidth: 1,
                        borderBottomColor: "primary.main",
                    }}
                    gutterBottom
                >
                    افزودن کاربر
                </DialogTitle>
                <DialogContent>
                    <TextField
                        variant="outlined"
                        label="نام"
                        placeholder="نام کاربر را وارد کنید"
                        margin="normal"
                        value={name}
                        onChange={(e) => setName(e.target.value)}
                        fullWidth
                    />
                    <TextField
                        variant="outlined"
                        label="شماره همراه"
                        placeholder="شماره همراه کاربر را وارد کنید"
                        margin="normal"
                        value={phone}
                        onChange={(e) => setPhone(e.target.value)}
                        fullWidth
                    />
                    <TextField
                        variant="outlined"
                        label="رمز"
                        placeholder="رمز کاربر را وارد کنید"
                        margin="normal"
                        type="password"
                        value={password}
                        onChange={(e) => setPassword(e.target.value)}
                        fullWidth
                    />
                    <FormControl margin="normal">
                        <FormLabel>جنسیت</FormLabel>
                        <RadioGroup
                            value={sex}
                            onChange={(e) => setSex(e.target.value)}
                            row
                        >
                            <FormControlLabel
                                value="male"
                                control={<Radio />}
                                label="آقا"
                            />
                            <FormControlLabel
                                value="femaile"
                                control={<Radio />}
                                label="خانم"
                            />
                        </RadioGroup>
                    </FormControl>
                    <br />
                    <FormControl margin="normal">
                        <FormLabel>دسترسی</FormLabel>
                        <RadioGroup
                            value={access}
                            onChange={(e) => setAccess(e.target.value)}
                            row
                        >
                            <FormControlLabel
                                value="ceo"
                                control={<Radio />}
                                label="مدیر عامل"
                            />
                            <FormControlLabel
                                value="admin"
                                control={<Radio />}
                                label="مدیر"
                            />
                            <FormControlLabel
                                value="viewer"
                                control={<Radio />}
                                label="بازرس"
                            />
                        </RadioGroup>
                    </FormControl>
                </DialogContent>
                <DialogActions>
                    <Button
                        variant="contained"
                        onClick={() => !loading && addUser()}
                        sx={{
                            ml: 2,
                            mb: 2,
                        }}
                        disabled={loading && true}
                        disableElevation
                    >
                        { loading ? "لطفا صبر کنید" : "افزودن کاربر" }
                    </Button>
                </DialogActions>
            </Dialog>
            
            <ShowSnackbar
                open={openSnack}
                close={() => setOpenSnack(false)}
                message={openSnackMessage}
            />
        </Box>
    );
}

export default AddUserDialog;