import { useState } from "react";

import {
    Dialog,
    DialogTitle,
    DialogContent,
    DialogActions,
    TextField,
    Button,
    Box,
    Divider,
} from "@mui/material";

import { MuiColorInput } from 'mui-color-input';

import Axios from "axios";

import ShowSnackbar from "../../components/snackbar.component";

const AddCityDialog = (props) => {
    const { open, close } = props;

    const env = process.env;
    const baseUrl = env.REACT_APP_BACKEND_URL;

    const [loading, setLoading] = useState(false);

    const [openSnack, setOpenSnack] = useState(false);
    const [openSnackMessage, setOpenSnackMessage] = useState("");

    const [name, setName] = useState("");
    const [color, setColor] = useState("#ffffff");

    const [budget, setBudget] = useState(0);
    
    const handleChangeColor = (color) => setColor(color);

    const addCity = () => {
        setLoading(true);

        const sendData = {
            name,
            color,
            budget,
        };

        Axios.post(`${baseUrl}/city/new`, sendData)
            .then((result) => {
                setName("");
                setColor("");

                close();

                setLoading(false);

                setOpenSnackMessage(result.data.message);
                setOpenSnack(true);
            })
            .catch((error) => {
                setOpenSnackMessage("متاسفانه مشکلی رخ داده است");
                setOpenSnack(true);

                setLoading(false);
            });
    }

    return (
        <Box>
            <Dialog
                open={open}
                onClose={close}
                sx={{
                    direction: "rtl",
                    textAlign: "right",
                }}
                maxWidth="sm"
                fullWidth
            >
                <DialogTitle
                    color="primary"
                    sx={{
                        color: "primary.main",
                        borderBottom: "solid",
                        borderBottomWidth: 1,
                        borderBottomColor: "primary.main",
                    }}
                    gutterBottom
                >
                    افزودن پروژه
                </DialogTitle>
                <DialogContent>
                    <TextField
                        variant="outlined"
                        label="نام پروژه"
                        placeholder="نام پروژه را وارد کنید"
                        margin="normal"
                        value={name}
                        onChange={(e) => setName(e.target.value)}
                        fullWidth
                    />
                    <MuiColorInput
                        margin="normal"
                        label="رنگ"
                        format="hex"
                        value={color}
                        onChange={handleChangeColor}
                        fullWidth
                    />
                    <Divider sx={{ my: 1 }}>
                        تخصیص بودجه ( میلیون ریال )
                    </Divider>
                    <TextField
                        variant="outlined"
                        label="بودجه تخصیص داده شده ( میلیون ریال )"
                        placeholder="بودجه اختصاص داده شده به این پروژه را وارد کنید"
                        value={budget}
                        margin="normal"
                        onChange={(e) => setBudget(e.target.value)}
                        fullWidth
                    />
                </DialogContent>
                <DialogActions>
                    <Button
                        variant="contained"
                        onClick={() => !loading && addCity()}
                        sx={{
                            ml: 2,
                            mb: 2,
                        }}
                        disabled={loading && true}
                        disableElevation
                    >
                        { loading ? "لطفا صبر کنید" : "افزودن پروژه" }
                    </Button>
                </DialogActions>
            </Dialog>
            
            <ShowSnackbar
                open={openSnack}
                close={() => setOpenSnack(false)}
                message={openSnackMessage}
            />
        </Box>
    );
}

export default AddCityDialog;