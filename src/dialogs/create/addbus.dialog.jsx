import { useState, useEffect } from "react";
import { useSelector } from "react-redux";

import {
    Dialog,
    DialogTitle,
    DialogContent,
    DialogActions,
    TextField,
    Button,
    Box,
    Autocomplete,
    Radio,
    RadioGroup,
    FormControl,
    FormLabel,
    FormControlLabel,
} from "@mui/material";

import DatePicker from "react-multi-date-picker"

import persian from "react-date-object/calendars/persian"
import persian_fa from "react-date-object/locales/persian_fa"

import Axios from "axios";

import ShowSnackbar from "../../components/snackbar.component";
import LoadingComponent from "../../components/loading.component";

const AddBusDialog = (props) => {
    const { open, close } = props;

    const env = process.env;
    const baseUrl = env.REACT_APP_BACKEND_URL;

    const [loading, setLoading] = useState(false);

    const [openSnack, setOpenSnack] = useState(false);
    const [openSnackMessage, setOpenSnackMessage] = useState("");

    const uid = useSelector(state => state.uid);

    const [name, setName] = useState("name");
    const [city, setCity] = useState("");
    const [status, setStatus] = useState("");
    const [gearbox, setGearbox] = useState("");
    const [chassis, setChassis] = useState("");
    const [plaque, setPlaque] = useState("");
    const [model, setModel] = useState("");
    const [delivered, setDelivered] = useState("");
    const [enter, setEnter] = useState(new Date());
    const [details, setDetails] = useState("");
    const [ac, setAc] = useState(false);

    const [cities, setCities] = useState("");
    const [statuses, setStatuses] = useState("");
    const [users, setUsers] = useState("");

    useEffect(() => {
        Axios.get(`${baseUrl}/city/all`)
            .then((result) => {
                setCities(result.data.cities);
            })
            .catch((error) => {
                setOpenSnackMessage("متاسفانه مشکلی رخ داده است");                
                setOpenSnack(true);
           });

        Axios.get(`${baseUrl}/status/all`)
           .then((result) => {
                setStatuses(result.data.statuses);
           })
           .catch((error) => {
                setOpenSnackMessage("متاسفانه مشکلی رخ داده است");                
                setOpenSnack(true);
          });

        Axios.post(`${baseUrl}/user/all`)
            .then((result) => {
                setUsers(result.data.users);
            })
            .catch((error) => {
                setOpenSnackMessage("متاسفانه مشکلی رخ داده است");                
                setOpenSnack(true);
            });
    }, [baseUrl]);

    const clearAndClose = () => {
        setName("name");
        setCity("");
        setStatus("");
        setGearbox("");
        setChassis("");
        setPlaque("");
        setDetails("");
        setAc(false);

        close();
    }

    const addUser = () => {
        setLoading(true);

        const date = new Date();

        const loggingData = {
            from: "اتوبوس جدید",
            to: statuses.filter((item) => item.name === status)[0].label,
            project: cities.filter((item) => item._id === city)[0].name,
            user: users.filter((item) => item._id === uid)[0].name,
            date: date.toLocaleDateString('fa-IR'),
            time: date.toLocaleTimeString('fa-IR'),
            bus: plaque,
        };

        const sendData = {
            data: {
                name,
                city,
                status,
                details,
                gearbox,
                chassis,
                plaque,
                model,
                delivered,
                enter,
                ac: ac === "true" ? true : false,
            },
            log: {
                ...loggingData,
                message: `اتوبوس ${loggingData.bus} در سیستم ساخته شد.`
            }
        };

        Axios.post(`${baseUrl}/bus/new`, sendData)
            .then((result) => {
                clearAndClose();
                setLoading(false);

                setOpenSnackMessage(result.data.message);
                setOpenSnack(true);
            })
            .catch((error) => {
                setOpenSnackMessage("متاسفانه مشکلی رخ داده است");                
                setOpenSnack(true);

                setLoading(false);
            });
    }

    return (
        <Box>
            <Dialog
                open={open}
                onClose={close}
                sx={{
                    direction: "rtl",
                    textAlign: "right",
                }}
                maxWidth="sm"
                fullWidth
            >
                <DialogTitle
                    color="primary"
                    sx={{
                        color: "primary.main",
                        borderBottom: "solid",
                        borderBottomWidth: 1,
                        borderBottomColor: "primary.main",
                    }}
                    gutterBottom
                >
                    افزودن اتوبوس
                </DialogTitle>
                {
                    cities !== "" && statuses !== ""
                    ?
                    <DialogContent>
                        <TextField
                            variant="outlined"
                            label="پلاک"
                            placeholder="پلاک اتوبوس را وارد کنید"
                            margin="normal"
                            value={plaque}
                            onChange={(e) => setPlaque(e.target.value)}
                            fullWidth
                        />
                        <Autocomplete
                            disablePortal
                            options={cities}
                            getOptionLabel={(option) => option.name}
                            onChange={(e, option) => setCity(option._id)}
                            renderInput={(params) => (
                                <TextField
                                    {...params}
                                    margin="normal"
                                    label="پروژه"
                                />
                            )}
                            fullWidth
                        />
                        <Autocomplete
                            disablePortal
                            options={statuses}
                            getOptionLabel={(option) => option.label}
                            onChange={(e, option) => setStatus(option.name)}
                            renderInput={(params) => (
                                <TextField
                                    {...params}
                                    margin="normal"
                                    label="وضعیت"
                                />
                            )}
                            fullWidth
                        />
                        <TextField
                            variant="outlined"
                            label="شماره موتور"
                            placeholder="شماره موتور اتوبوس را وارد کنید"
                            margin="normal"
                            value={gearbox}
                            onChange={(e) => setGearbox(e.target.value)}
                            fullWidth
                        />
                        <TextField
                            variant="outlined"
                            label="شماره شاسی"
                            placeholder="شماره شاسی اتوبوس را وارد کنید"
                            margin="normal"
                            value={chassis}
                            onChange={(e) => setChassis(e.target.value)}
                            fullWidth
                        />
                        <TextField
                            variant="outlined"
                            label="تاریخ ورود"
                            placeholder="تاریخ ورود اتوبوس را وارد کنید"
                            margin="normal"
                            value={enter}
                            onChange={(e) => setEnter(e.target.value)}
                            fullWidth
                        />
                        <DatePicker
                            value={enter}
                            onChange={setEnter}
                            calendar={persian}
                            locale={persian_fa}
                            // render={<TextField />}
                        />
                        <TextField
                            variant="outlined"
                            label="تاریخ خروج"
                            placeholder="تاریخ خروج اتوبوس را وارد کنید"
                            margin="normal"
                            value={delivered}
                            onChange={(e) => setDelivered(e.target.value)}
                            fullWidth
                        />
                        <TextField
                            variant="outlined"
                            label="مدل"
                            placeholder="مدل اتوبوس را وارد کنید"
                            margin="normal"
                            value={model}
                            onChange={(e) => setModel(e.target.value)}
                            fullWidth
                        />
                        <TextField
                            variant="outlined"
                            label="توضیحات"
                            placeholder="توضیحات اتوبوس را وارد کنید"
                            margin="normal"
                            value={details}
                            onChange={(e) => setDetails(e.target.value)}
                            rows={5}
                            multiline
                            fullWidth
                        />
                        <FormControl margin="normal">
                            <FormLabel>کولر</FormLabel>
                            <RadioGroup
                                value={ac}
                                onChange={(e) => setAc(e.target.value)}
                                row
                            >
                                <FormControlLabel
                                    value="true"
                                    control={<Radio />}
                                    label="دارد"
                                />
                                <FormControlLabel
                                    value="false"
                                    control={<Radio />}
                                    label="ندارد"
                                />
                            </RadioGroup>
                        </FormControl>
                    </DialogContent>
                    :
                    <DialogContent>
                        <LoadingComponent />
                    </DialogContent>
                }
                <DialogActions>
                    <Button
                        variant="contained"
                        onClick={() => !loading && addUser()}
                        sx={{
                            ml: 2,
                            mb: 2,
                        }}
                        disabled={loading && true}
                        disableElevation
                    >
                        { loading ? "لطفا صبر کنید" : "افزودن اتوبوس" }
                    </Button>
                </DialogActions>
            </Dialog>
            
            <ShowSnackbar
                open={openSnack}
                close={() => setOpenSnack(false)}
                message={openSnackMessage}
            />
        </Box>
    );
}

export default AddBusDialog;