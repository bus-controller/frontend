import { useState } from "react";

import {
    Dialog,
    DialogTitle,
    DialogContent,
    DialogActions,
    TextField,
    Button,
    Box,
} from "@mui/material";

import Axios from "axios";

import ShowSnackbar from "../../components/snackbar.component";

const AddStatusDialog = (props) => {
    const { open, close } = props;

    const env = process.env;
    const baseUrl = env.REACT_APP_BACKEND_URL;

    const [loading, setLoading] = useState(false);

    const [openSnack, setOpenSnack] = useState(false);
    const [openSnackMessage, setOpenSnackMessage] = useState("");

    const [name, setName] = useState("");
    const [label, setLabel] = useState("");

    const addCity = () => {
        setLoading(true);

        const sendData = {
            name: name.toLowerCase(),
            label,
        };

        Axios.post(`${baseUrl}/status/new`, sendData)
            .then((result) => {
                setName("");
                setLabel("");

                close();

                setLoading(false);

                setOpenSnackMessage(result.data.message);
                setOpenSnack(true);
            })
            .catch((error) => {
                setOpenSnackMessage("متاسفانه مشکلی رخ داده است");
                setOpenSnack(true);

                setLoading(false);
            });
    }

    return (
        <Box>
            <Dialog
                open={open}
                onClose={close}
                sx={{
                    direction: "rtl",
                    textAlign: "right",
                }}
                maxWidth="sm"
                fullWidth
            >
                <DialogTitle
                    color="primary"
                    sx={{
                        color: "primary.main",
                        borderBottom: "solid",
                        borderBottomWidth: 1,
                        borderBottomColor: "primary.main",
                    }}
                    gutterBottom
                >
                    افزودن وضعیت
                </DialogTitle>
                <DialogContent>
                    <TextField
                        variant="outlined"
                        label="نام وضعیت"
                        placeholder="نام وضعیت را به فارسی وارد کنید"
                        margin="normal"
                        value={label}
                        onChange={(e) => setLabel(e.target.value)}
                        fullWidth
                    />
                    <TextField
                        variant="outlined"
                        label="نام انگلیسی"
                        placeholder="نام وضعیت را به انگلیسی وارد کنید"
                        margin="normal"
                        value={name}
                        onChange={(e) => setName(e.target.value)}
                        fullWidth
                    />
                </DialogContent>
                <DialogActions>
                    <Button
                        variant="contained"
                        onClick={() => !loading && addCity()}
                        sx={{
                            ml: 2,
                            mb: 2,
                        }}
                        disabled={loading && true}
                        disableElevation
                    >
                        { loading ? "لطفا صبر کنید" : "افزودن وضعیت" }
                    </Button>
                </DialogActions>
            </Dialog>
            
            <ShowSnackbar
                open={openSnack}
                close={() => setOpenSnack(false)}
                message={openSnackMessage}
            />
        </Box>
    );
}

export default AddStatusDialog;