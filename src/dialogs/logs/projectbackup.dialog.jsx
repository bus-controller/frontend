import { useEffect, useState } from "react";
import { useHistory } from "react-router-dom";

import {
    Dialog,
    DialogTitle,
    DialogContent,
    List,
    ListItem,
    ListItemButton,
    ListItemText,
    Box,
    DialogContentText,
} from "@mui/material";

import Axios from "axios";

import ShowSnackbar from "../../components/snackbar.component";
import LoadingComponent from "../../components/loading.component";

const ProjectBackup = (props) => {
    const { open, close } = props;

    const history = useHistory();

    const env = process.env;
    const baseUrl = env.REACT_APP_BACKEND_URL;

    const [openSnack, setOpenSnack] = useState(false);
    const [openSnackMessage, setOpenSnackMessage] = useState("");

    const [projects, setProjects] = useState("");

    useEffect(() => {
        Axios.get(`${baseUrl}/logs/backup/project/list`)
            .then((result) => {
                setProjects(result.data.projects);
            })
            .catch((error) => {
                setOpenSnackMessage("متاسفانه مشکلی رخ داده است");                
                setOpenSnack(true);
            });
    }, [baseUrl]);

    return (
        <Box>
            <Dialog
                open={open}
                onClose={close}
                sx={{
                    direction: "rtl",
                    textAlign: "right",
                }}
                maxWidth="sm"
                fullWidth
            >
                <DialogTitle
                    color="primary"
                    sx={{
                        color: "primary.main",
                        borderBottom: "solid",
                        borderBottomWidth: 1,
                        borderBottomColor: "primary.main",
                    }}
                    gutterBottom
                >
                    گزارش گیری پروژه ها
                </DialogTitle>
                <DialogContent>
                    <DialogContentText sx={{ pt: 1 }}>
                        پروژه مورد نظر را انتخاب کنید:
                    </DialogContentText>
                    {
                        projects !== ""
                        ?
                        <List>
                            {
                                projects.map((project) => (
                                    <ListItem
                                        key={project._id}
                                        disablePadding
                                        divider
                                    >
                                        <ListItemButton
                                            onClick={() => { history.push(`/home/report/project/${project._id}`); close(); }}
                                        >
                                            <ListItemText
                                                primary={project.name}
                                                sx={{
                                                    textAlign: "right",
                                                }}
                                            />
                                        </ListItemButton>
                                    </ListItem>
                                ))                                
                            }
                        </List>
                        :
                        <LoadingComponent />
                    }
                </DialogContent>
            </Dialog>
            
            <ShowSnackbar
                open={openSnack}
                close={() => setOpenSnack(false)}
                message={openSnackMessage}
            />
        </Box>
    );
}

export default ProjectBackup;