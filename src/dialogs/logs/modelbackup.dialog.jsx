import { useEffect, useState } from "react";

import {
    Dialog,
    DialogTitle,
    DialogContent,
    List,
    ListItem,
    ListItemButton,
    ListItemText,
    Box,
    DialogContentText,
} from "@mui/material";

import Axios from "axios";
import * as XLSX from "xlsx";

import ShowSnackbar from "../../components/snackbar.component";
import LoadingComponent from "../../components/loading.component";

const ModelBackup = (props) => {
    const { open, close } = props;

    const env = process.env;
    const baseUrl = env.REACT_APP_BACKEND_URL;

    const [loading, setLoading] = useState(false);

    const [openSnack, setOpenSnack] = useState(false);
    const [openSnackMessage, setOpenSnackMessage] = useState("");

    const [models, setModels] = useState("");

    useEffect(() => {
        Axios.get(`${baseUrl}/logs/backup/model/list`)
            .then((result) => {
                setModels(result.data.models);
            })
            .catch((error) => {
                setOpenSnackMessage("متاسفانه مشکلی رخ داده است");                
                setOpenSnack(true);
            });
    }, [baseUrl]);

    const getBackupDatabase = (model) => {
        setLoading(true);

        Axios.post(`${baseUrl}/logs/backup/model`, { model })
            .then((result) => {
                const { data, naming, message } = result.data;

                setLoading(false);

                const wb = XLSX.utils.book_new();
                const ws = XLSX.utils.json_to_sheet(data.reverse());
        
                XLSX.utils.book_append_sheet(wb, ws, "backup");
                XLSX.writeFile(wb, `${naming}.xlsx`);

                setOpenSnackMessage(message);
                setOpenSnack(true);
            })
            .catch((error) => {
                setLoading(false);
                
                setOpenSnackMessage("متاسفانه بک‌آپ گرفته نشد");
                setOpenSnack(true);
            });
    }

    return (
        <Box>
            <Dialog
                open={open}
                onClose={close}
                sx={{
                    direction: "rtl",
                    textAlign: "right",
                }}
                maxWidth="sm"
                fullWidth
            >
                <DialogTitle
                    color="primary"
                    sx={{
                        color: "primary.main",
                        borderBottom: "solid",
                        borderBottomWidth: 1,
                        borderBottomColor: "primary.main",
                    }}
                    gutterBottom
                >
                    بک‌آپ گیری از مدل ها
                </DialogTitle>
                <DialogContent>
                    <DialogContentText sx={{ pt: 1 }}>
                        بخش مورد نظر را انتخاب کنید:
                    </DialogContentText>
                    {
                        models !== ""
                        ?
                        <List>
                            {
                                loading
                                ?
                                <LoadingComponent />
                                :
                                models.map((model) => (
                                    <ListItem
                                        key={model}
                                        disablePadding
                                        divider
                                    >
                                        <ListItemButton
                                            onClick={() => getBackupDatabase(model)}
                                        >
                                            <ListItemText primary={model} />
                                        </ListItemButton>
                                    </ListItem>
                                ))
                            }
                        </List>
                        :
                        <LoadingComponent />
                    }
                </DialogContent>
            </Dialog>
            
            <ShowSnackbar
                open={openSnack}
                close={() => setOpenSnack(false)}
                message={openSnackMessage}
            />
        </Box>
    );
}

export default ModelBackup;