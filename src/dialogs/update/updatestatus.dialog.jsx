import { useState, useEffect } from "react";

import {
    Dialog,
    DialogTitle,
    DialogContent,
    DialogActions,
    TextField,
    Button,
    Box,
    Grid,
} from "@mui/material";

import {
    KeyboardArrowDown,
    KeyboardArrowUp,
} from "@mui/icons-material";

import Axios from "axios";

import ShowSnackbar from "../../components/snackbar.component";
import LoadingComponent from "../../components/loading.component";

const UpdateStatusDialog = (props) => {
    const { open, close, id } = props;

    const env = process.env;
    const baseUrl = env.REACT_APP_BACKEND_URL;

    const [loading, setLoading] = useState(false);

    const [openSnack, setOpenSnack] = useState(false);
    const [openSnackMessage, setOpenSnackMessage] = useState("");

    const [name, setName] = useState("");
    const [label, setLabel] = useState("");
    const [sort, setSort] = useState("");

    const [status, setStatus] = useState("");

    useEffect(() => {
        if (id) {
            Axios.get(`${baseUrl}/status/one/${id}`)
                .then((result) => {
                    const status = result.data.status;

                    setStatus(status);
                    
                    setName(status.name);
                    setLabel(status.label);
                    setSort(status.sort);
                })
                .catch((error) => {
                    setOpenSnackMessage("متاسفانه مشکلی رخ داده است");                
                    setOpenSnack(true);
                });
        }
    }, [baseUrl, id]);

    const clearAndClose = () => {
        setName("");
        setLabel("");
        setSort("");

        close();
    }

    const updateStatus = () => {
        setLoading(true);

        const sendData = {
            id,
            data: {
                name,
                label,
                sort,
            },
        };

        Axios.post(`${baseUrl}/status/update`, sendData)
            .then((result) => {
                clearAndClose();
                setLoading(false);

                setOpenSnackMessage(result.data.message);
                setOpenSnack(true);
            })
            .catch((error) => {
                setOpenSnackMessage("متاسفانه مشکلی رخ داده است");
                setOpenSnack(true);

                setLoading(false);
            });
    }

    const updateOrder = (type) => {
        setOpenSnackMessage(type);
            setOpenSnack(true);
    }

    const deleteStatus = () => {
        setLoading(true);

        Axios.delete(`${baseUrl}/status/delete/${id}`)
            .then((result) => {
                clearAndClose();
                setLoading(false);

                setOpenSnackMessage(result.data.message);
                setOpenSnack(true);
            })
            .catch((error) => {
                setOpenSnackMessage("متاسفانه مشکلی رخ داده است");
                setOpenSnack(true);

                setLoading(false);
            });
    }

    return (
        <Box>
            <Dialog
                open={open}
                onClose={close}
                sx={{
                    direction: "rtl",
                    textAlign: "right",
                }}
                maxWidth="sm"
                fullWidth
            >
                <DialogTitle
                    color="primary"
                    sx={{
                        color: "primary.main",
                        borderBottom: "solid",
                        borderBottomWidth: 1,
                        borderBottomColor: "primary.main",
                    }}
                    gutterBottom
                >
                    آپدیت وضعیت
                </DialogTitle>
                {
                    status !== ""
                    ?
                    <DialogContent>
                        <TextField
                            variant="outlined"
                            label="نام وضعیت"
                            placeholder="نام وضعیت را به فارسی وارد کنید"
                            margin="normal"
                            value={label}
                            onChange={(e) => setLabel(e.target.value)}
                            fullWidth
                        />
                        <TextField
                            variant="outlined"
                            label="نام انگلیسی"
                            placeholder="نام وضعیت را به انگلیسی وارد کنید"
                            margin="normal"
                            value={name}
                            onChange={(e) => setName(e.target.value)}
                            fullWidth
                        />
                        <TextField
                            variant="outlined"
                            label="زدیف"
                            placeholder="ردیف وارد کنید"
                            margin="normal"
                            value={sort}
                            onChange={(e) => setSort(e.target.value)}
                            fullWidth
                        />
                        <Grid
                            spacing={2}
                            sx={{
                                mt: 0.5,
                            }}
                            container
                        >
                            <Grid
                                md={6}
                                item
                            >
                                <Button
                                    variant="outlined"
                                    size="large"
                                    endIcon={<KeyboardArrowUp sx={{ mr: 2 }} />}
                                    disabled={loading && true}
                                    onClick={() => !loading && updateOrder("up")}
                                    fullWidth
                                >
                                    { loading ? "لطفا صبر کنید" : "یکی بالا" }
                                </Button>
                            </Grid>
                            <Grid
                                md={6}
                                item
                            >
                                <Button
                                    variant="outlined"
                                    size="large"
                                    endIcon={<KeyboardArrowDown sx={{ mr: 2 }} />}
                                    disabled={loading && true}
                                    onClick={() => !loading && updateOrder("down")}
                                    fullWidth
                                >
                                    { loading ? "لطفا صبر کنید" : "یکی پایین" }
                                </Button>
                            </Grid>
                        </Grid>
                    </DialogContent>
                    :
                    <DialogContent>
                        <LoadingComponent />
                    </DialogContent>
                }
                <DialogActions>
                    <Button
                        variant="contained"
                        color="error"
                        size="large"
                        onClick={() => !loading && deleteStatus()}
                        sx={{
                            ml: 1,
                            mb: 2,
                        }}
                        disabled={loading && true}
                        disableElevation
                    >
                        { loading ? "لطفا صبر کنید" : "حذف وضعیت" }
                    </Button>
                    <Button
                        variant="contained"
                        size="large"
                        onClick={() => !loading && updateStatus()}
                        sx={{
                            ml: 2,
                            mb: 2,
                        }}
                        disabled={loading && true}
                        disableElevation
                    >
                        { loading ? "لطفا صبر کنید" : "آپدیت وضعیت" }
                    </Button>
                </DialogActions>
            </Dialog>
            
            <ShowSnackbar
                open={openSnack}
                close={() => setOpenSnack(false)}
                message={openSnackMessage}
            />
        </Box>
    );
}

export default UpdateStatusDialog;