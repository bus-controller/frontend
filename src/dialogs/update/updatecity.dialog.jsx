import { useState, useEffect } from "react";

import {
    Dialog,
    DialogTitle,
    DialogContent,
    DialogActions,
    TextField,
    Button,
    Box,
} from "@mui/material";

import { MuiColorInput } from 'mui-color-input';

import Axios from "axios";

import ShowSnackbar from "../../components/snackbar.component";
import LoadingComponent from "../../components/loading.component";

const UpdateCityDialog = (props) => {
    const { open, close, id } = props;

    const env = process.env;
    const baseUrl = env.REACT_APP_BACKEND_URL;

    const [loading, setLoading] = useState(false);

    const [openSnack, setOpenSnack] = useState(false);
    const [openSnackMessage, setOpenSnackMessage] = useState("");

    const [name, setName] = useState("");
    const [color, setColor] = useState("#ffffff");

    const handleChangeColor = (color) => setColor(color);

    const [city, setCity] = useState("");

    useEffect(() => {
        if (id) {
            Axios.get(`${baseUrl}/city/one/${id}`)
                .then((result) => {
                    const city = result.data.city;

                    setCity(city);
                    
                    setName(city.name);
                    setColor(city.color);
                })
                .catch((error) => {
                    setOpenSnackMessage("متاسفانه مشکلی رخ داده است");                
                    setOpenSnack(true);
                });
        }
    }, [baseUrl, id]);

    const clearAndClose = () => {
        setName("");
        setColor("");

        close();
    }

    const updateCity = () => {
        setLoading(true);

        const sendData = {
            id,
            data: {
                name,
                color,
            },
        };

        Axios.post(`${baseUrl}/city/update`, sendData)
            .then((result) => {
                clearAndClose();
                setLoading(false);

                setOpenSnackMessage(result.data.message);
                setOpenSnack(true);
            })
            .catch((error) => {
                setOpenSnackMessage("متاسفانه مشکلی رخ داده است");
                setOpenSnack(true);

                setLoading(false);
            });
    }

    const deleteCity = () => {
        setLoading(true);

        Axios.delete(`${baseUrl}/city/delete/${id}`)
            .then((result) => {
                clearAndClose();
                setLoading(false);

                setOpenSnackMessage(result.data.message);
                setOpenSnack(true);
            })
            .catch((error) => {
                setOpenSnackMessage("متاسفانه مشکلی رخ داده است");
                setOpenSnack(true);

                setLoading(false);
            });
    }

    return (
        <Box>
            <Dialog
                open={open}
                onClose={close}
                sx={{
                    direction: "rtl",
                    textAlign: "right",
                }}
                maxWidth="sm"
                fullWidth
            >
                <DialogTitle
                    color="primary"
                    sx={{
                        color: "primary.main",
                        borderBottom: "solid",
                        borderBottomWidth: 1,
                        borderBottomColor: "primary.main",
                    }}
                    gutterBottom
                >
                    آپدیت پروژه
                </DialogTitle>
                {
                    city !== ""
                    ?
                    <DialogContent>
                        <TextField
                            variant="outlined"
                            label="نام پروژه"
                            placeholder="نام پروژه را وارد کنید"
                            margin="normal"
                            value={name}
                            onChange={(e) => setName(e.target.value)}
                            fullWidth
                        />
                        <MuiColorInput
                            margin="normal"
                            label="رنگ"
                            format="hex"
                            value={color}
                            onChange={handleChangeColor}
                            fullWidth
                        />
                    </DialogContent>
                    :
                    <DialogContent>
                        <LoadingComponent />
                    </DialogContent>
                }
                <DialogActions>
                    <Button
                        variant="contained"
                        color="error"
                        size="large"
                        onClick={() => !loading && deleteCity()}
                        sx={{
                            ml: 1,
                            mb: 2,
                        }}
                        disabled={loading && true}
                        disableElevation
                    >
                        { loading ? "لطفا صبر کنید" : "حذف پروژه" }
                    </Button>
                    <Button
                        variant="contained"
                        size="large"
                        onClick={() => !loading && updateCity()}
                        sx={{
                            ml: 2,
                            mb: 2,
                        }}
                        disabled={loading && true}
                        disableElevation
                    >
                        { loading ? "لطفا صبر کنید" : "آپدیت پروژه" }
                    </Button>
                </DialogActions>
            </Dialog>
            
            <ShowSnackbar
                open={openSnack}
                close={() => setOpenSnack(false)}
                message={openSnackMessage}
            />
        </Box>
    );
}

export default UpdateCityDialog;