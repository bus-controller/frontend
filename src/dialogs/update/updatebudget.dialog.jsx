import { useState, useEffect } from "react";

import {
    Dialog,
    DialogTitle,
    DialogContent,
    DialogActions,
    TextField,
    Button,
    Box,
    Grid,
    Divider,
    Table,
    TableContainer,
    TableHead,
    TableBody,
    TableRow,
    TableCell,
    Paper,
} from "@mui/material";

// import { NumericFormat } from 'react-number-format';

import Axios from "axios";

import ShowSnackbar from "../../components/snackbar.component";
import LoadingComponent from "../../components/loading.component";
import { persianNumeric } from "../../hooks/most.hook";

const UpdateBudgetDialog = (props) => {
    const { open, close, id } = props;

    const env = process.env;
    const baseUrl = env.REACT_APP_BACKEND_URL;

    const [loading, setLoading] = useState(false);

    const [openSnack, setOpenSnack] = useState(false);
    const [openSnackMessage, setOpenSnackMessage] = useState("");

    const [budget, setBudget] = useState(0);

    // eslint-disable-next-line
    const [spent, setSpent] = useState(0);

    const [s, setS] = useState(0);
    
    const [sazehBudget, setSazehBudget] = useState(0);
    const [niroBudget, setNiroBudget] = useState(0);
    const [barghBudget, setBarghBudget] = useState(0);
    const [tazinatBudget, setTazinatBudget] = useState(0);
    const [colorBudget, setColorBudget] = useState(0);
    const [etcBudget, setEtcBudget] = useState(0);

    const [sazehBudgetSpent, setSazehBudgetSpent] = useState(0);
    const [niroBudgetSpent, setNiroBudgetSpent] = useState(0);
    const [barghBudgetSpent, setBarghBudgetSpent] = useState(0);
    const [tazinatBudgetSpent, setTazinatBudgetSpent] = useState(0);
    const [colorBudgetSpent, setColorBudgetSpent] = useState(0);
    const [etcBudgetSpent, setEtcBudgetSpent] = useState(0);

    const [city, setCity] = useState("");

    useEffect(() => {
        if (id) {
            Axios.get(`${baseUrl}/city/one/${id}`)
                .then((result) => {
                    const city = result.data.city;

                    setCity(city);
                    
                    setBudget(city.budget);
                    setSpent(
                        city.sazehBudgetSpent + city.niroBudgetSpent + city.barghBudgetSpent + city.tazinatBudgetSpent + city.colorBudgetSpent + city.etcBudgetSpent
                    );

                    setS(
                        city.sazehBudgetSpent + city.niroBudgetSpent + city.barghBudgetSpent + city.tazinatBudgetSpent + city.colorBudgetSpent + city.etcBudgetSpent
                    );

                    setSazehBudget(city.sazehBudget);
                    setNiroBudget(city.niroBudget);
                    setBarghBudget(city.barghBudget);
                    setTazinatBudget(city.tazinatBudget);
                    setColorBudget(city.colorBudget);
                    setEtcBudget(city.etcBudget);

                    setSazehBudgetSpent(city.sazehBudgetSpent);
                    setNiroBudgetSpent(city.niroBudgetSpent);
                    setBarghBudgetSpent(city.barghBudgetSpent);
                    setTazinatBudgetSpent(city.tazinatBudgetSpent);
                    setColorBudgetSpent(city.colorBudgetSpent);
                    setEtcBudgetSpent(city.colorBudgetSpent);
                })
                .catch((error) => {
                    setOpenSnackMessage("متاسفانه مشکلی رخ داده است");                
                    setOpenSnack(true);
                });
        }
    }, [baseUrl, id]);

    const clearAndClose = () => {
        setBudget(0);
        setSpent(0);


        setSazehBudget(0);
        setNiroBudget(0);
        setBarghBudget(0);
        setTazinatBudget(0);
        setColorBudget(0);
        setEtcBudget(0);

        setSazehBudgetSpent(0);
        setNiroBudgetSpent(0);
        setBarghBudgetSpent(0);
        setTazinatBudgetSpent(0);
        setColorBudgetSpent(0);
        setEtcBudgetSpent(0);

        close();
    }

    const updateAllBudget = () => {
        setLoading(true);

        const sendData = {
            id,
            data: {
                budget,
            },
        };

        Axios.post(`${baseUrl}/city/update`, sendData)
            .then((result) => {
                clearAndClose();
                setLoading(false);

                setOpenSnackMessage("بودجه کل با موفقیت آپدیت شد");
                setOpenSnack(true);
            })
            .catch((error) => {
                setOpenSnackMessage("متاسفانه مشکلی رخ داده است");
                setOpenSnack(true);

                setLoading(false);
            });
    }

    const updateAllUnitsBudgets = () => {
        setLoading(true);

        const allSet = Number(sazehBudget) + Number(niroBudget) + Number(barghBudget) + Number(tazinatBudget) + Number(colorBudget) + Number(etcBudget);

        if (Number(budget) === allSet) {
            const sendData = {
                id,
                data: {
                    sazehBudget,
                    niroBudget,
                    barghBudget,
                    tazinatBudget,
                    colorBudget,
                    etcBudget,
                },
            };
    
            Axios.post(`${baseUrl}/city/update`, sendData)
                .then((result) => {
                    clearAndClose();
                    setLoading(false);
    
                    setOpenSnackMessage("بودجه تخصیص داده شده به واحد ها آپدیت شد");
                    setOpenSnack(true);
                })
                .catch((error) => {
                    setOpenSnackMessage("متاسفانه مشکلی رخ داده است");
                    setOpenSnack(true);
    
                    setLoading(false);
                });
        } else {
            setLoading(false);

            setOpenSnackMessage("مقادیر وارد شده را دوباره چک کنید");
            setOpenSnack(true);
        }
    }

    const updateSpentUnitsBudgets = () => {
        setLoading(true);

        if (
            sazehBudgetSpent <= sazehBudget &&
            niroBudgetSpent <= niroBudget &&
            barghBudgetSpent <= barghBudget &&
            tazinatBudgetSpent <= tazinatBudget &&
            colorBudgetSpent <= colorBudget &&
            etcBudgetSpent <= etcBudget
        ) {
            const sendData = {
                id,
                data: {
                    sazehBudgetSpent,
                    niroBudgetSpent,
                    barghBudgetSpent,
                    tazinatBudgetSpent,
                    colorBudgetSpent,
                    etcBudgetSpent,
                },
            };
    
            Axios.post(`${baseUrl}/city/update`, sendData)
                .then((result) => {
                    clearAndClose();
                    setLoading(false);
    
                    setOpenSnackMessage("بودجه مصرف شده واحد ها آپدیت شد");
                    setOpenSnack(true);
                })
                .catch((error) => {
                    setOpenSnackMessage("متاسفانه مشکلی رخ داده است");
                    setOpenSnack(true);
    
                    setLoading(false);
                });
        } else {
            const messages = [];

            if (sazehBudgetSpent > sazehBudget) messages.push(`مقدار مصرف شده سازه بیشتر از بودجه اصلی است\n`);
            if (niroBudgetSpent > niroBudget) messages.push(`مقدار مصرف شده نیرو محرکه بیشتر از بودجه اصلی است\n`);
            if (barghBudgetSpent > barghBudget) messages.push(`مقدار مصرف شده برق بیشتر از بودجه اصلی است\n`);
            if (tazinatBudgetSpent > tazinatBudget) messages.push(`مقدار مصرف شده نزئینات بیشتر از بودجه اصلی است\n`);
            if (colorBudgetSpent > colorBudget) messages.push(`مقدار مصرف شده رنگ بیشتر از بودجه اصلی است\n`);
            if (etcBudgetSpent > etcBudget) messages.push(`مقدار مصرف شده غیره بیشتر از بودجه اصلی است\n`);
            
            setLoading(false);

            setOpenSnackMessage(messages);
            setOpenSnack(true);
        }
    }

    return (
        <Box>
            <Dialog
                open={open}
                onClose={close}
                sx={{
                    direction: "rtl",
                    textAlign: "right",
                }}
                maxWidth="lg"
                fullWidth
            >
                <DialogTitle
                    color="primary"
                    sx={{
                        color: "primary.main",
                        borderBottom: "solid",
                        borderBottomWidth: 1,
                        borderBottomColor: "primary.main",
                    }}
                    gutterBottom
                >
                    آپدیت بودجه پروژه
                </DialogTitle>
                {
                    city !== ""
                    ?
                    <DialogContent>
                        <Grid
                            spacing={2}
                            container
                        >
                            <Grid md={6} item>
                                <Divider sx={{ my: 2 }}>
                                    بودجه تخصیص داده شده ( میلیون ریال )
                                </Divider>
                                <TextField
                                    variant="outlined"
                                    label="بودجه کامل اختصاص داده شده"
                                    placeholder="بودجه اختصاص داده شده به این پروژه را وارد کنید"
                                    value={budget}
                                    onChange={(e) => setBudget(e.target.value)}
                                    fullWidth
                                />
                                {/* <NumericFormat
                                    label="بودجه کامل اختصاص داده شده"
                                    placeholder="بودجه اختصاص داده شده به این پروژه را وارد کنید"
                                    customInput={TextField}
                                    allowLeadingZeros
                                    thousandSeparator=","
                                    value={budget.formattedValue}
                                    onValueChange={(values, sourceInfo) => setBudget(values)}
                                    fullWidth
                                /> */}
                                <Button
                                    variant="contained"
                                    size="large"
                                    onClick={() => !loading && updateAllBudget()}
                                    sx={{
                                        mt: 1,
                                    }}
                                    disabled={loading && true}
                                    disableElevation
                                    fullWidth
                                >
                                    { loading ? "لطفا صبر کنید" : "آپدیت بودجه تخصیص داده شده" }
                                </Button>
                                <Divider sx={{ my: 2 }}>
                                    جدول گزارش
                                </Divider>
                                <TableContainer
                                    component={Paper}
                                    variant="outlined"
                                    sx={{
                                        borderColor: "primary.main"
                                    }}
                                >
                                    <Table>
                                        <TableHead>
                                            <TableRow>
                                                <TableCell colSpan={4} align="center" sx={{ bgcolor: city.color, color: "black", fontWeight: "bold" }}>{ city.name }</TableCell>
                                            </TableRow>
                                            <TableRow>
                                                <TableCell colSpan={2} align="center">کل بودجه مصوب</TableCell>
                                                <TableCell colSpan={2} align="center">{ persianNumeric(city.budget) }</TableCell>
                                            </TableRow>
                                        </TableHead>
                                        <TableBody>
                                            <TableRow>
                                                <TableCell align="center">شرح</TableCell>
                                                <TableCell align="center">بودجه مصوب</TableCell>
                                                <TableCell align="center">هزینه تا کنون</TableCell>
                                                <TableCell align="center">مانده</TableCell>
                                            </TableRow>
                                            <TableRow>
                                                <TableCell align="center">سازه</TableCell>
                                                <TableCell align="center">{ persianNumeric(city.sazehBudget) }</TableCell>
                                                <TableCell align="center">{ persianNumeric(city.sazehBudgetSpent) }</TableCell>
                                                <TableCell align="center">{ persianNumeric(city.sazehBudget - city.sazehBudgetSpent) }</TableCell>
                                            </TableRow>
                                            <TableRow>
                                                <TableCell align="center">نیرو محرکه</TableCell>
                                                <TableCell align="center">{ persianNumeric(city.niroBudget) }</TableCell>
                                                <TableCell align="center">{ persianNumeric(city.niroBudgetSpent) }</TableCell>
                                                <TableCell align="center">{ persianNumeric(city.niroBudget - city.niroBudgetSpent) }</TableCell>
                                            </TableRow>
                                            <TableRow>
                                                <TableCell align="center">برق</TableCell>
                                                <TableCell align="center">{ persianNumeric(city.barghBudget) }</TableCell>
                                                <TableCell align="center">{ persianNumeric(city.barghBudgetSpent) }</TableCell>
                                                <TableCell align="center">{ persianNumeric(city.barghBudget - city.barghBudgetSpent) }</TableCell>
                                            </TableRow>
                                            <TableRow>
                                                <TableCell align="center">تزئینات</TableCell>
                                                <TableCell align="center">{ persianNumeric(city.tazinatBudget) }</TableCell>
                                                <TableCell align="center">{ persianNumeric(city.tazinatBudgetSpent) }</TableCell>
                                                <TableCell align="center">{ persianNumeric(city.tazinatBudget - city.tazinatBudgetSpent) }</TableCell>
                                            </TableRow>
                                            <TableRow>
                                                <TableCell align="center">رنگ</TableCell>
                                                <TableCell align="center">{ persianNumeric(city.colorBudget) }</TableCell>
                                                <TableCell align="center">{ persianNumeric(city.colorBudgetSpent) }</TableCell>
                                                <TableCell align="center">{ persianNumeric(city.colorBudget - city.colorBudgetSpent) }</TableCell>
                                            </TableRow>
                                            <TableRow>
                                                <TableCell align="center">غیره</TableCell>
                                                <TableCell align="center">{ persianNumeric(city.etcBudget) }</TableCell>
                                                <TableCell align="center">{ persianNumeric(city.etcBudgetSpent) }</TableCell>
                                                <TableCell align="center">{ persianNumeric(city.etcBudget - city.etcBudgetSpent) }</TableCell>
                                            </TableRow>
                                            <TableRow>
                                                <TableCell align="center" sx={{ color: "red", fontWeight: "bold", fontSize: "120%" }}>جمع کل</TableCell>
                                                <TableCell align="center" sx={{ color: "red", fontWeight: "bold", fontSize: "120%" }}>{ persianNumeric(city.budget) }</TableCell>
                                                <TableCell align="center" sx={{ color: "red", fontWeight: "bold", fontSize: "120%" }}>{ persianNumeric(s) }</TableCell>
                                                <TableCell align="center" sx={{ color: "red", fontWeight: "bold", fontSize: "120%" }}>{ persianNumeric(city.budget - s) }</TableCell>
                                            </TableRow>
                                        </TableBody>
                                    </Table>
                                </TableContainer>
                            </Grid>
                            <Grid md={6} item>
                                <Divider sx={{ my: 2 }}>
                                    تخصیص بودجه هر بخش ( میلیون ریال )
                                </Divider>
                                <Grid
                                    spacing={2}
                                    container
                                >
                                    <Grid md={6} item>
                                        <TextField
                                            variant="outlined"
                                            label="سازه"
                                            placeholder="بودجه اختصاص داده شده به سازه را وارد کنید"
                                            value={sazehBudget}
                                            onChange={(e) => setSazehBudget(e.target.value)}
                                            fullWidth
                                        />
                                    </Grid>
                                    <Grid md={6} item>
                                        <TextField
                                            variant="outlined"
                                            label="نیرو محرکه"
                                            placeholder="بودجه اختصاص داده شده به نیرو محرکه را وارد کنید"
                                            value={niroBudget}
                                            onChange={(e) => setNiroBudget(e.target.value)}
                                            fullWidth
                                        />
                                    </Grid>
                                    <Grid md={6} item>
                                        <TextField
                                            variant="outlined"
                                            label="برق"
                                            placeholder="بودجه اختصاص داده شده به برق را وارد کنید"
                                            value={barghBudget}
                                            onChange={(e) => setBarghBudget(e.target.value)}
                                            fullWidth
                                        />
                                    </Grid>
                                    <Grid md={6} item>
                                        <TextField
                                            variant="outlined"
                                            label="تزئینات"
                                            placeholder="بودجه اختصاص داده شده به تزئینات را وارد کنید"
                                            value={tazinatBudget}
                                            onChange={(e) => setTazinatBudget(e.target.value)}
                                            fullWidth
                                        />
                                    </Grid>
                                    <Grid md={6} item>
                                        <TextField
                                            variant="outlined"
                                            label="رنگ"
                                            placeholder="بودجه اختصاص داده شده به رنگ را وارد کنید"
                                            value={colorBudget}
                                            onChange={(e) => setColorBudget(e.target.value)}
                                            fullWidth
                                        />
                                    </Grid>
                                    <Grid md={6} item>
                                        <TextField
                                            variant="outlined"
                                            label="غیره"
                                            placeholder="بودجه اختصاص داده شده به موارد غیره را وارد کنید"
                                            value={etcBudget}
                                            onChange={(e) => setEtcBudget(e.target.value)}
                                            fullWidth
                                        />
                                    </Grid>
                                    <Grid md={12} item>
                                        <Button
                                            variant="contained"
                                            size="large"
                                            onClick={() => !loading && updateAllUnitsBudgets()}
                                            disabled={loading && true}
                                            disableElevation
                                            fullWidth
                                        >
                                            { loading ? "لطفا صبر کنید" : "آپدیت بودجه تخصیص داده شده هر بخش" }
                                        </Button>
                                    </Grid>
                                </Grid>
                                <Divider sx={{ my: 2 }}>
                                    بودجه مصرف شده تا کنون ( میلیون ریال )
                                </Divider>
                                <Grid
                                    spacing={2}
                                    container
                                >
                                    <Grid md={6} item>
                                        <TextField
                                            variant="outlined"
                                            label="سازه"
                                            placeholder="هزینه مصرف شده سازه را وارد کنید"
                                            value={sazehBudgetSpent}
                                            onChange={(e) => setSazehBudgetSpent(e.target.value)}
                                            fullWidth
                                        />
                                    </Grid>
                                    <Grid md={6} item>
                                        <TextField
                                            variant="outlined"
                                            label="نیرو محرکه"
                                            placeholder="هزینه مصرف شده نیرو محرکه را وارد کنید"
                                            value={niroBudgetSpent}
                                            onChange={(e) => setNiroBudgetSpent(e.target.value)}
                                            fullWidth
                                        />
                                    </Grid>
                                    <Grid md={6} item>
                                        <TextField
                                            variant="outlined"
                                            label="برق"
                                            placeholder="هزینه مصرف شده برق را وارد کنید"
                                            value={barghBudgetSpent}
                                            onChange={(e) => setBarghBudgetSpent(e.target.value)}
                                            fullWidth
                                        />
                                    </Grid>
                                    <Grid md={6} item>
                                        <TextField
                                            variant="outlined"
                                            label="تزئینات"
                                            placeholder="هزینه مصرف شده تزئینات را وارد کنید"
                                            value={tazinatBudgetSpent}
                                            onChange={(e) => setTazinatBudgetSpent(e.target.value)}
                                            fullWidth
                                        />
                                    </Grid>
                                    <Grid md={6} item>
                                        <TextField
                                            variant="outlined"
                                            label="رنگ"
                                            placeholder="هزینه مصرف شده رنگ را وارد کنید"
                                            value={colorBudgetSpent}
                                            onChange={(e) => setColorBudgetSpent(e.target.value)}
                                            fullWidth
                                        />
                                    </Grid>
                                    <Grid md={6} item>
                                        <TextField
                                            variant="outlined"
                                            label="غیره"
                                            placeholder="هزینه مصرف شده رنگ را وارد کنید"
                                            value={etcBudgetSpent}
                                            onChange={(e) => setEtcBudgetSpent(e.target.value)}
                                            fullWidth
                                        />
                                    </Grid>
                                    <Grid md={12} item>
                                        <Button
                                            variant="contained"
                                            size="large"
                                            onClick={() => !loading && updateSpentUnitsBudgets()}
                                            disabled={loading && true}
                                            disableElevation
                                            fullWidth
                                        >
                                            { loading ? "لطفا صبر کنید" : "آپدیت بودجه مصرف شده تا کنون" }
                                        </Button>
                                    </Grid>
                                </Grid>
                            </Grid>
                        </Grid>
                    </DialogContent>
                    :
                    <DialogContent>
                        <LoadingComponent />
                    </DialogContent>
                }
                <DialogActions>
                    <Button
                        variant="contained"
                        size="large"
                        onClick={() => close()}
                        sx={{
                            ml: 2,
                            mb: 2,
                        }}
                        disableElevation
                    >
                        خروج
                    </Button>
                </DialogActions>
            </Dialog>
            
            <ShowSnackbar
                open={openSnack}
                close={() => setOpenSnack(false)}
                message={openSnackMessage}
            />
        </Box>
    );
}

export default UpdateBudgetDialog;