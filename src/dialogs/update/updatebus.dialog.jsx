import { useState, useEffect } from "react";
import { useSelector } from "react-redux";

import {
    Dialog,
    DialogTitle,
    DialogContent,
    DialogActions,
    TextField,
    Button,
    Box,
    Autocomplete,
    Radio,
    RadioGroup,
    FormControl,
    FormLabel,
    FormControlLabel,
} from "@mui/material";

import Axios from "axios";

import ShowSnackbar from "../../components/snackbar.component";
import LoadingComponent from "../../components/loading.component";

const UpdateBusDialog = (props) => {
    const { open, close, id } = props;

    const env = process.env;
    const baseUrl = env.REACT_APP_BACKEND_URL;

    const [loading, setLoading] = useState(false);

    const [openSnack, setOpenSnack] = useState(false);
    const [openSnackMessage, setOpenSnackMessage] = useState("");

    const [name, setName] = useState("name");
    const [status, setStatus] = useState("");
    const [gearbox, setGearbox] = useState("");
    const [chassis, setChassis] = useState("");
    const [plaque, setPlaque] = useState("");
    const [model, setModel] = useState("");
    const [delivered, setDelivered] = useState("");
    const [enter, setEnter] = useState("");
    const [details, setDetails] = useState("");
    const [ceoCommad, setCeoCommad] = useState("");
    const [ac, setAc] = useState(false);

    const [bus, setBus] = useState("");

    const access = useSelector(state => state.user);
    const uid = useSelector(state => state.uid);

    const [cities, setCities] = useState("");
    const [statuses, setStatuses] = useState("");
    const [users, setUsers] = useState("");

    useEffect(() => {
        if (id) {
            Axios.get(`${baseUrl}/bus/one/${id}`)
                .then((result) => {
                    const bus = result.data.bus;

                    setBus(bus);
                    setName(bus.name);
                    setStatus(bus.status);
                    setGearbox(bus.gearbox);
                    setChassis(bus.chassis);
                    setPlaque(bus.plaque);
                    setAc(bus.ac);
                    setEnter(bus.enter);
                    setDelivered(bus.delivered);
                    setModel(bus.model);
                    setCeoCommad(bus.ceo_command);
                    setDetails(bus.details);
                })
                .catch((error) => {
                    setOpenSnackMessage("متاسفانه مشکلی رخ داده است");                
                    setOpenSnack(true);
            });

            Axios.get(`${baseUrl}/city/all`)
                .then((result) => {
                    setCities(result.data.cities);
                })
                .catch((error) => {
                    setOpenSnackMessage("متاسفانه مشکلی رخ داده است");                
                    setOpenSnack(true);
                });

            Axios.get(`${baseUrl}/status/all`)
                .then((result) => {
                        setStatuses(result.data.statuses);
                })
                .catch((error) => {
                    setOpenSnackMessage("متاسفانه مشکلی رخ داده است");                
                    setOpenSnack(true);
                });

            Axios.post(`${baseUrl}/user/all`)
                .then((result) => {
                    setUsers(result.data.users);
                })
                .catch((error) => {
                    setOpenSnackMessage("متاسفانه مشکلی رخ داده است");                
                    setOpenSnack(true);
                });
        }
    }, [baseUrl, id]);

    const clearAndClose = () => {
        setName("name");
        setStatus("");
        setGearbox("");
        setChassis("");
        setPlaque("");
        setDetails("");
        setEnter("");
        setDelivered("");
        setModel("");
        setCeoCommad("");
        setAc(false);

        close();
    }

    const updateBus = () => {
        setLoading(true);

        const date = new Date();

        const loggingData = {
            from: statuses.filter((item) => item.name === bus.status)[0].label,
            to: statuses.filter((item) => item.name === status)[0].label,
            project: cities.filter((item) => item._id === bus.city)[0].name,
            user: users.filter((item) => item._id === uid)[0].name,
            date: date.toLocaleDateString('fa-IR'),
            time: date.toLocaleTimeString('fa-IR'),
            bus: bus.plaque,
        };

        const sendData = {
            id,
            data: {
                name,
                status,
                details,
                gearbox,
                chassis,
                plaque,
                model,
                enter,
                delivered,
                ceo_command: ceoCommad,
                ac: ac === "true" ? true : false,
            },
            log: {
                ...loggingData,
                message: `اتوبوس ${loggingData.bus} از بخش ${loggingData.from} به بخش ${loggingData.to} منتقل شد.`
            }
        };

        Axios.post(`${baseUrl}/bus/update`, sendData)
            .then((result) => {
                clearAndClose();
                setLoading(false);

                setOpenSnackMessage(result.data.message);
                setOpenSnack(true);
            })
            .catch((error) => {
                setOpenSnackMessage("متاسفانه مشکلی رخ داده است");
                setOpenSnack(true);

                setLoading(false);
            });
    }

    const deleteBus = () => {
        setLoading(true);

        const date = new Date();

        const loggingData = {
            from: statuses.filter((item) => item.name === bus.status)[0].label,
            to: "سطل زباله",
            project: cities.filter((item) => item._id === bus.city)[0].name,
            user: users.filter((item) => item._id === uid)[0].name,
            date: date.toLocaleDateString('fa-IR'),
            time: date.toLocaleTimeString('fa-IR'),
            bus: bus.plaque,
        };

        const sendingData = {
            id,
            log: {
                ...loggingData,
                message: `اتوبوس ${loggingData.bus} حذف گردید.`
            }
        };

        Axios.post(`${baseUrl}/bus/delete`, sendingData)
            .then((result) => {
                clearAndClose();
                setLoading(false);

                setOpenSnackMessage(result.data.message);
                setOpenSnack(true);
            })
            .catch((error) => {
                setOpenSnackMessage("متاسفانه مشکلی رخ داده است");
                setOpenSnack(true);

                setLoading(false);
            });
    }

    const restoreBus = () => {
        setLoading(true);

        const date = new Date();

        const loggingData = {
            from: "سطل زباله",
            to: statuses.filter((item) => item.name === bus.status)[0].label,
            project: cities.filter((item) => item._id === bus.city)[0].name,
            user: users.filter((item) => item._id === uid)[0].name,
            date: date.toLocaleDateString('fa-IR'),
            time: date.toLocaleTimeString('fa-IR'),
            bus: bus.plaque,
        };

        const sendingData = {
            id,
            log: {
                ...loggingData,
                message: `اتوبوس ${loggingData.bus} به سیستم باز گردید.`
            }
        };

        Axios.post(`${baseUrl}/bus/restore`, sendingData)
            .then((result) => {
                clearAndClose();
                setLoading(false);

                setOpenSnackMessage(result.data.message);
                setOpenSnack(true);
            })
            .catch((error) => {
                setOpenSnackMessage("متاسفانه مشکلی رخ داده است");
                setOpenSnack(true);

                setLoading(false);
            });
    }

    return (
        <Box>
            <Dialog
                open={open}
                onClose={close}
                sx={{
                    direction: "rtl",
                    textAlign: "right",
                }}
                maxWidth="sm"
                fullWidth
            >
                <DialogTitle
                    color="primary"
                    sx={{
                        color: "primary.main",
                        borderBottom: "solid",
                        borderBottomWidth: 1,
                        borderBottomColor: "primary.main",
                    }}
                    gutterBottom
                >
                    آپدیت اتوبوس
                </DialogTitle>
                {
                    bus !== "" && cities !== "" && statuses !== ""
                    ?
                    <DialogContent>
                        <TextField
                            variant="outlined"
                            label="پلاک"
                            placeholder="پلاک اتوبوس را وارد کنید"
                            margin="normal"
                            value={plaque}
                            onChange={(e) => setPlaque(e.target.value)}
                            fullWidth
                        />
                        <Autocomplete
                            disablePortal
                            options={statuses}
                            getOptionLabel={(option) => option.label}
                            onChange={(e, option) => setStatus(option.name)}
                            renderInput={(params) => (
                                <TextField
                                    {...params}
                                    margin="normal"
                                    label="تغییر وضعیت"
                                />
                            )}
                            fullWidth
                        />
                        <TextField
                            variant="outlined"
                            label="شماره موتور"
                            placeholder="شماره موتور اتوبوس را وارد کنید"
                            margin="normal"
                            value={gearbox}
                            onChange={(e) => setGearbox(e.target.value)}
                            fullWidth
                        />
                        <TextField
                            variant="outlined"
                            label="شماره شاسی"
                            placeholder="شماره شاسی اتوبوس را وارد کنید"
                            margin="normal"
                            value={chassis}
                            onChange={(e) => setChassis(e.target.value)}
                            fullWidth
                        />
                        <TextField
                            variant="outlined"
                            label="تاریخ ورود"
                            placeholder="تاریخ ورود اتوبوس را وارد کنید"
                            margin="normal"
                            value={enter}
                            onChange={(e) => setEnter(e.target.value)}
                            fullWidth
                        />
                        <TextField
                            variant="outlined"
                            label="تاریخ خروج"
                            placeholder="تاریخ خروج اتوبوس را وارد کنید"
                            margin="normal"
                            value={delivered}
                            onChange={(e) => setDelivered(e.target.value)}
                            fullWidth
                        />
                        <TextField
                            variant="outlined"
                            label="مدل"
                            placeholder="مدل اتوبوس را وارد کنید"
                            margin="normal"
                            value={model}
                            onChange={(e) => setModel(e.target.value)}
                            fullWidth
                        />
                        <TextField
                            variant="outlined"
                            label="توضیحات"
                            placeholder="توضیحات اتوبوس را وارد کنید"
                            margin="normal"
                            value={details}
                            onChange={(e) => setDetails(e.target.value)}
                            rows={5}
                            multiline
                            fullWidth
                        />
                        {
                            access === "ceo"
                            &&
                            <TextField
                                variant="outlined"
                                color="error"
                                label="دستور حاص"
                                placeholder="دستورات را وارد کنید"
                                margin="normal"
                                value={ceoCommad}
                                onChange={(e) => setCeoCommad(e.target.value)}
                                rows={5}
                                multiline
                                fullWidth
                            />
                        }
                        <FormControl margin="normal">
                            <FormLabel>کولر</FormLabel>
                            <RadioGroup
                                value={ac}
                                onChange={(e) => setAc(e.target.value)}
                                row
                            >
                                <FormControlLabel
                                    value="true"
                                    control={<Radio />}
                                    label="دارد"
                                />
                                <FormControlLabel
                                    value="false"
                                    control={<Radio />}
                                    label="ندارد"
                                />
                            </RadioGroup>
                        </FormControl>
                    </DialogContent>
                    :
                    <DialogContent>
                        <LoadingComponent />
                    </DialogContent>
                }
                <DialogActions>
                    <Button
                        variant="contained"
                        color={ bus.deleted ? "success" : "error" }
                        size="large"
                        onClick={() => !loading && bus.deleted ? restoreBus() : deleteBus()}
                        sx={{
                            ml: 1,
                            mb: 2,
                        }}
                        disabled={loading && true}
                        disableElevation
                    >
                        { loading ? "لطفا صبر کنید" : bus.deleted ? "بازگردانی" : "حذف اتوبوس" }
                    </Button>
                    <Button
                        variant="contained"
                        size="large"
                        onClick={() => !loading && updateBus()}
                        sx={{
                            ml: 2,
                            mb: 2,
                        }}
                        disabled={loading && true}
                        disableElevation
                    >
                        { loading ? "لطفا صبر کنید" : "آپدیت اتوبوس" }
                    </Button>
                </DialogActions>
            </Dialog>
            
            <ShowSnackbar
                open={openSnack}
                close={() => setOpenSnack(false)}
                message={openSnackMessage}
            />
        </Box>
    );
}

export default UpdateBusDialog;