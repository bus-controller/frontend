import { useState } from "react";
import { useSelector, useDispatch } from "react-redux";

import { MuiColorInput } from 'mui-color-input';

import {
    Dialog,
    DialogTitle,
    DialogContent,
    Box,
} from "@mui/material";

import ShowSnackbar from "../../components/snackbar.component";
import { setColor } from "../../redux/actions/color";

const PaletteDialog = (props) => {
    const { open, close } = props;

    const dispatch = useDispatch();

    const colorThemeRedux = useSelector(state => state.color);

    const [openSnack, setOpenSnack] = useState(false);
    const [openSnackMessage, setOpenSnackMessage] = useState("");

    const [hex, setHex] = useState(colorThemeRedux);
    const handleChangeColor = (color) => {
        dispatch(setColor(color));
        setHex(color);
    };

    return (
        <Box>
            <Dialog
                open={open}
                onClose={close}
                sx={{
                    direction: "rtl",
                    textAlign: "right",
                }}
                maxWidth="sm"
                fullWidth
            >
                <DialogTitle
                    color="primary"
                    sx={{
                        color: "primary.main",
                        borderBottom: "solid",
                        borderBottomWidth: 1,
                        borderBottomColor: "primary.main",
                    }}
                    gutterBottom
                >
                    تغییر رنگ تم
                </DialogTitle>
                <DialogContent>
                    <MuiColorInput
                        margin="normal"
                        label="رنگ"
                        format="hex"
                        value={hex}
                        onChange={handleChangeColor}
                        fullWidth
                    />
                </DialogContent>
            </Dialog>
            
            <ShowSnackbar
                open={openSnack}
                close={() => setOpenSnack(false)}
                message={openSnackMessage}
            />
        </Box>
    );
}

export default PaletteDialog;