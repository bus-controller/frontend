import {
    CircularProgress,
    Box,
} from "@mui/material";

const LoadingComponent = () => {
    return (
        <Box
            sx={{
                textAlign: "center",
                pt: 5,
            }}
        >
            <CircularProgress />
        </Box>
    );
}

export default LoadingComponent;