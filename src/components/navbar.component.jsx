import { useDispatch, useSelector } from "react-redux";
import { useHistory } from "react-router-dom";
import { useState, useEffect } from "react";

import {
    Box,
    Typography,
    Toolbar,
    Drawer,
    AppBar,
    IconButton,
} from "@mui/material";

import {
    Menu,
    DarkMode,
    LightMode,
} from "@mui/icons-material";

import Axios from "axios";

import { setTheme } from "../redux/actions/theme";

import ShowSnackbar from "./snackbar.component";
import DrawerComponent from "./drawer.component";

const drawerWith = 300;

const Navbar = () => {
    const dispatch = useDispatch();
    const history = useHistory();

    const session = useSelector(state => state.session);
    if (!session) history.push("/auth");

    const id = useSelector(state => state.uid);
    const mode = useSelector(state => state.theme);

    // eslint-disable-next-line
    const [user, setUser] = useState("");

    const [openSnack, setOpenSnack] = useState(false);
    const [openSnackMessage, setOpenSnackMessage] = useState("");

    const env = process.env;
    const baseUrl = env.REACT_APP_BACKEND_URL;

    const [drawerOpen, setDrawerOpen] = useState(false);
    const closeDrawer = () => setDrawerOpen(false);

    useEffect(() => {
        Axios.get(`${baseUrl}/user/user/${id}`)
            .then((result) => {
                setUser(result.data.user);
                
                setOpenSnack(true);
                setOpenSnackMessage("خوش آمدید");
            })
            .catch((error) => {
                setOpenSnack(true);
                setOpenSnackMessage("خطای سرور");
            });
    }, [baseUrl, id]);

    return (
        <Box>
            <AppBar
                elevation={0}
            >
                <Toolbar>
                    <IconButton
                        color="inherit"
                        onClick={() => setDrawerOpen(!drawerOpen)}
                        sx={{
                            ml: 2,
                        }}
                    >
                        <Menu />
                    </IconButton>
                    <Typography
                        variant="h6"
                        onClick={() => history.push('/')}
                        sx={{
                            cursor: "pointer",
                            flexGrow: 1,
                        }}
                    >
                        وضعیت اتوبوس های در حال بازسازی شهاب خودرو
                    </Typography>
                    <IconButton
                        color="inherit"
                        onClick={() => dispatch(setTheme(mode === 'light' ? 'dark' : 'light'))}
                    >
                        { mode === 'light' ? <DarkMode /> : <LightMode />}
                    </IconButton>
                </Toolbar>
            </AppBar>

            <Box
                component="nav"
            >
                <Drawer
                    variant="temporary"
                    anchor="right"
                    open={drawerOpen}
                    onClose={() => setDrawerOpen(false)}
                    ModalProps={{
                        keepMounted: true,
                    }}
                    sx={{
                        '& .MuiDrawer-paper': {
                            boxSizing: 'border-box',
                            width: drawerWith,
                            bgcolor: "background.default",
                            direction: "rtl"
                        },
                    }}
                >
                    <Box>
                        <DrawerComponent close={() => closeDrawer()} />
                    </Box>
                </Drawer>
            </Box>

            <ShowSnackbar
                open={openSnack}
                close={() => setOpenSnack(false)}
                message={openSnackMessage}
            />
        </Box>
    );
}

export default Navbar;