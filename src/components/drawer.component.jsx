import { useDispatch, useSelector } from "react-redux";
import { useHistory } from "react-router-dom";
import { useState } from "react";

import {
    Box,
    Button,
    List,
    ListItemButton,
    ListItemIcon,
    ListItemText,
    Collapse,
    Typography,
    Divider,
    Toolbar,
} from "@mui/material";

import {
    Public,
    DirectionsBus,
    Hub,
    Home,
    TravelExplore,
    Logout,
    TextFields,
    PlaylistAdd,
    ExpandLess,
    ExpandMore,
    People,
    PersonAdd,
    Equalizer,
    AccountTree,
    Login,
    WebStories,
    Print,
    Delete,
    DeleteSweep,
    Backup,
    FolderCopy,
    Storage,
    Settings,
    Palette,
} from "@mui/icons-material";

import { logoutUser } from "../redux/actions/session";
import { unsertAccess } from "../redux/actions/user";
import { unsetUID } from "../redux/actions/uid";

import AddStatusDialog from "../dialogs/create/addstatus.dialog";
import AddUserDialog from "../dialogs/create/adduser.dialog";
import AddCityDialog from "../dialogs/create/addcity.dialog";
import AddBusDialog from "../dialogs/create/addbus.dialog";
import ModelBackup from "../dialogs/logs/modelbackup.dialog";

import Axios from "axios";
import ProjectBackup from "../dialogs/logs/projectbackup.dialog";
import PaletteDialog from "../dialogs/settings/palette.dialog";

import ShowSnackbar from "./snackbar.component";

const DrawerComponent = (props) => {
    const { close } = props;

    const env = process.env;
    const baseUrl = env.REACT_APP_BACKEND_URL;

    const dispatch = useDispatch();
    const history = useHistory();

    const user = useSelector(state => state.user);
    const uid = useSelector(state => state.uid);

    const [openSnack, setOpenSnack] = useState(false);
    const [openSnackMessage, setOpenSnackMessage] = useState("");

    const [openUsers, setOpenUsers] = useState(false);
    const [openStatus, setOpenStatus] = useState(false);
    const [openProject, setOpenProject] = useState(false);
    const [openPalette, setOpenPalette] = useState(false);
    const [openTrashcan, setOpenTrashcan] = useState(false);
    const [openBackupModel, setOpenBackupModel] = useState(false);
    const [openBackupProject, setOpenBackupProject] = useState(false);

    const [openAddUser, setOpenAddUser] = useState(false);
    const [openAddBus, setOpenAddBus] = useState(false);
    const [openAddCity, setOpenAddCity] = useState(false);
    const [openAddStatus, setOpenAddStatus] = useState(false);
    const [openReport, setOpenReport] = useState(false);
    const [openBackup, setOpenBackup] = useState(false);
    const [openSettings, setOpenSettings] = useState(false);

    const viewerItems = [
        {
            type: "single",
            item: {
                text: "خانه",
                icon: <Home />,
                action: () => { history.push("/home"); close(); },
            },
        },
        {
            type: "nested",
            header: {
                text: "تنظیمات",
                icon: <Settings  color="primary" />,
                action: () => setOpenSettings(!openSettings),
                open: openSettings,
            },
            items: [
                {
                    text: "شخصی سازی تم",
                    icon: <Palette color="primary" />,
                    action: () => setOpenPalette(true),
                },
            ],
        },
    ];

    const newItems = [
        {
            type: "single",
            item: {
                text: "خانه",
                icon: <Home />,
                action: () => { history.push("/home"); close(); },
            },
        },
        {
            type: "nested",
            header: {
                text: "کاربران",
                icon: <People />,
                action: () => setOpenUsers(!openUsers),
                open: openUsers,
            },
            items: [
                {
                    text: "دسترسی ها",
                    icon: <Hub />,
                    action: () => { history.push("/home/access"); close(); },
                },
                {
                    text: "افزودن کاربر",
                    icon: <PersonAdd />,
                    action: () => setOpenAddUser(true),
                },
            ],
        },
        {
            type: "nested",
            header: {
                text: "وضعیت",
                icon: <Equalizer />,
                action: () => setOpenStatus(!openStatus),
                open: openStatus,
            },
            items: [
                {
                    text: "وضعیت ها",
                    icon: <TextFields />,
                    action: () => { history.push("/home/categories"); close(); },
                },
                {
                    text: "افزودن وضعیت",
                    icon: <PlaylistAdd />,
                    action: () => setOpenAddStatus(true),
                },
            ],
        },
        {
            type: "nested",
            header: {
                text: "پروژه",
                icon: <AccountTree color="primary" />,
                action: () => setOpenProject(!openProject),
                open: openProject,
            },
            items: [
                {
                    text: "پروژه ها",
                    icon: <TravelExplore color="primary" />,
                    action: () => { history.push("/home/projects"); close(); },
                },
                {
                    text: "افزودن پروژه",
                    icon: <Public color="primary" />,
                    action: () => setOpenAddCity(true),
                },
                {
                    text: "افزودن اتوبوس",
                    icon: <DirectionsBus color="primary" />,
                    action: () => setOpenAddBus(true),
                },
            ],
        },
        {
            type: "nested",
            header: {
                text: "سطل های زباله",
                icon: <Delete color="primary" />,
                action: () => setOpenTrashcan(!openTrashcan),
                open: openTrashcan,
            },
            items: [
                {
                    text: "اتوبوس ها",
                    icon: <DeleteSweep color="primary" />,
                    action: () => { history.push("/home/trash"); close(); },
                },
            ],
        },
        {
            type: "nested",
            header: {
                text: "گزارشات",
                icon: <WebStories color="primary" />,
                action: () => setOpenReport(!openReport),
                open: openReport,
            },
            items: [
                {
                    text: "گزارش ورودی ها",
                    icon: <Login color="primary" />,
                    action: () => { history.push("/home/logs/login"); close(); },
                },
                {
                    text: "گزارش اتوبوس ها",
                    icon: <Print color="primary" />,
                    action: () => { history.push("/home/logs/bus"); close(); },
                },
                {
                    text: "گزارش گیری پروژه ها",
                    icon: <FolderCopy color="primary" />,
                    action: () => setOpenBackupProject(true),
                },
            ],
        },
        {
            type: "nested",
            header: {
                text: "بک‌آپ",
                icon: <Backup color="primary" />,
                action: () => setOpenBackup(!openBackup),
                open: openBackup,
            },
            items: [
                {
                    text: "بک‌آپ مدل ها",
                    icon: <Storage color="primary" />,
                    action: () => setOpenBackupModel(true),
                },
            ],
        },
        {
            type: "nested",
            header: {
                text: "تنظیمات",
                icon: <Settings  color="primary" />,
                action: () => setOpenSettings(!openSettings),
                open: openSettings,
            },
            items: [
                {
                    text: "شخصی سازی تم",
                    icon: <Palette color="primary" />,
                    action: () => setOpenPalette(true),
                },
            ],
        },
    ];

    const logout = () => {
        Axios.post(`${baseUrl}/auth/logout`, { uid })
            .then((result) => {
                dispatch(logoutUser());
                dispatch(unsetUID());
                dispatch(unsertAccess());
        
                history.push("/");
            })
            .catch((error) => {
                setOpenSnackMessage("متاسفانه مشکلی رخ داده است");                
                setOpenSnack(true);
            });
    }

    return (
        <Box
            sx={{
                textAlign: 'center',
            }}
        >
            <Toolbar>
                <Box sx={{ textAlign: "center", py: 1 }}>
                    <Box
                        src="https://www.shahabkhodro.co.ir/wp-content/themes/shahabkhodro/assets/images/logo.png"
                        component="img"
                        sx={{
                            height: 80,
                        }}
                    />
                    <Typography
                        variant="h6"
                        fontWeight="bold"
                        gutterBottom
                    >
                        وضعیت اتوبوس های در حال بازسازی شهاب خودرو                                            
                    </Typography>
                </Box>
            </Toolbar>
            <Divider />
            <List>
                {
                    user === "viewer"
                    ?
                    viewerItems.map((item) => (
                        item.type === "nested"
                        ?
                        <Box key={item}>
                            <ListItemButton onClick={item.header.action}>
                                <ListItemIcon sx={{ color: "primary.main" }}>
                                    { item.header.icon }
                                </ListItemIcon>
                                <ListItemText
                                    sx={{
                                        textAlign: "right",
                                        color: "primary.main"
                                    }}
                                    primary={item.header.text}
                                />
                                { item.header.open ? <ExpandLess /> : <ExpandMore /> }
                            </ListItemButton>
                            <Collapse in={item.header.open} timeout="auto" unmountOnExit>
                                <List component="div" disablePadding>
                                    {
                                        item.items.map((i) => (
                                            <ListItemButton
                                                key={i.text}
                                                onClick={i.action}
                                                sx={{ pr: 4 }}
                                            >
                                                <ListItemIcon sx={{ color: "primary.main" }}>
                                                    { i.icon }
                                                </ListItemIcon>
                                                <ListItemText
                                                    sx={{
                                                        textAlign: "right",
                                                        color: "primary.main"
                                                    }}
                                                    primary={i.text}
                                                />
                                            </ListItemButton>
                                        ))
                                    }
                                </List>
                            </Collapse>
                        </Box>
                        :
                        <ListItemButton
                            onClick={item.item.action}
                        >
                            <ListItemIcon sx={{ color: "primary.main" }}>
                                { item.item.icon }
                            </ListItemIcon>
                            <ListItemText
                                sx={{
                                    textAlign: "right",
                                    color: "primary.main"
                                }}
                                primary={item.item.text}
                            />
                        </ListItemButton>
                    ))
                    :
                    newItems.map((item) => (
                        item.type === "nested"
                        ?
                        <Box key={item}>
                            <ListItemButton onClick={item.header.action}>
                                <ListItemIcon sx={{ color: "primary.main" }}>
                                    { item.header.icon }
                                </ListItemIcon>
                                <ListItemText
                                    sx={{
                                        textAlign: "right",
                                        color: "primary.main"
                                    }}
                                    primary={item.header.text}
                                />
                                { item.header.open ? <ExpandLess /> : <ExpandMore /> }
                            </ListItemButton>
                            <Collapse in={item.header.open} timeout="auto" unmountOnExit>
                                <List component="div" disablePadding>
                                    {
                                        item.items.map((i) => (
                                            <ListItemButton
                                                key={i.text}
                                                onClick={i.action}
                                                sx={{ pr: 4 }}
                                            >
                                                <ListItemIcon sx={{ color: "primary.main" }}>
                                                    { i.icon }
                                                </ListItemIcon>
                                                <ListItemText
                                                    sx={{
                                                        textAlign: "right",
                                                        color: "primary.main"
                                                    }}
                                                    primary={i.text}
                                                />
                                            </ListItemButton>
                                        ))
                                    }
                                </List>
                            </Collapse>
                        </Box>
                        :
                        <ListItemButton
                            onClick={item.item.action}
                        >
                            <ListItemIcon sx={{ color: "primary.main" }}>
                                { item.item.icon }
                            </ListItemIcon>
                            <ListItemText
                                sx={{
                                    textAlign: "right",
                                    color: "primary.main"
                                }}
                                primary={item.item.text}
                            />
                        </ListItemButton>
                    ))
                }
            </List>
            <Divider />
            <Box
                sx={{
                    m: 2
                }}
            >
                <Button
                    variant="outlined"
                    size="large"
                    sx={{
                        p: 2,
                    }}
                    startIcon={<Logout sx={{ ml: 2 }} />}
                    onClick={() => logout()}
                    fullWidth
                >
                    خروج از حساب
                </Button>
            </Box>

            <ShowSnackbar
                open={openSnack}
                close={() => setOpenSnack(false)}
                message={openSnackMessage}
            />

            <ProjectBackup open={openBackupProject} close={() => setOpenBackupProject(false)} />
            <ModelBackup open={openBackupModel} close={() => setOpenBackupModel(false)} />
            <AddStatusDialog open={openAddStatus} close={() => setOpenAddStatus(false)} />
            <PaletteDialog open={openPalette} close={() => setOpenPalette(false)} />
            <AddCityDialog open={openAddCity} close={() => setOpenAddCity(false)} />
            <AddUserDialog open={openAddUser} close={() => setOpenAddUser(false)} />
            <AddBusDialog open={openAddBus} close={() => setOpenAddBus(false)} />
        </Box>
    );
}

export default DrawerComponent;