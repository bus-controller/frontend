export const persianNumeric = (data) => {
    return data.toLocaleString('fa-IR');
}