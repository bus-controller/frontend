import {
  createTheme,
  CssBaseline,
  ThemeProvider,
  colors,
  Box,
} from "@mui/material";
import { faIR } from '@mui/material/locale';

import { useSelector } from "react-redux";

import {
  BrowserRouter as Router,
  Switch,
  Route,
} from "react-router-dom";

import AuthenticationPage from "./pages/auth/authentication.page.";
import HomePage from "./pages/home/home.page";

import StartTab from "./pages/home/tabs/start.tab";
import AccessTab from "./pages/home/tabs/access.tab";
import CitiesTab from "./pages/home/tabs/cities.tab";
import StatusesTab from "./pages/home/tabs/status.tab";

import AuthenticationProvider from "./pages/authentication.provider";

import BusLogsTab from "./pages/home/tabs/busreport.tab";
import LoginLogTab from "./pages/home/tabs/loginreport.tab";
import DeletedBusesTab from "./pages/home/tabs/trashcan.tab";
import ReportLogTab from "./pages/home/tabs/projectreport.tab";

function App() {
  const mode = useSelector(state => state.theme);
  const color = useSelector(state => state.color);

  const session = useSelector(state => state.session);

  const theme = createTheme({
    palette: {
      mode: mode,
      background: {
        default: mode === "dark" ? "#333" : colors.blue[50],
      },
      primary: {
        main: mode === "dark" ? colors.blue[200] : color,
      }
    },
    typography: {
      fontFamily: "iransans, Vazirmatn, Titr",
    },
  }, faIR);
  
  return (
    <ThemeProvider theme={theme}>
      <CssBaseline />
      <Box
        sx={{
          direction: "rtl",
          textAlign: "right",
        }}
      >
        <Router>
          <AuthenticationProvider>
            {
              session
              ?
              <HomePage>
                <Switch>
                  <Route path={"/"} exact><StartTab /></Route>

                  <Route path={"/home"} exact><StartTab /></Route>
                  <Route path={"/home/trash"} exact><DeletedBusesTab /></Route>

                  <Route path={"/home/report/project/:project_id"} exact><ReportLogTab /></Route>

                  <Route path={"/home/projects"} exact><CitiesTab /></Route>
                  <Route path={"/home/categories"} exact><StatusesTab /></Route>
                  <Route path={"/home/access"} exact><AccessTab /></Route>

                  <Route path={"/home/logs/bus"} exact><BusLogsTab /></Route>
                  <Route path={"/home/logs/login"} exact><LoginLogTab /></Route>
                </Switch>
              </HomePage>
              :
              <Switch>
                <Route path={"/"} exact><AuthenticationPage /></Route>
                <Route path={"/auth"} exact><AuthenticationPage /></Route>
              </Switch>
            }
          </AuthenticationProvider>
        </Router>
      </Box>
    </ThemeProvider>
  );
}

export default App;

